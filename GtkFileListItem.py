from os.path import isdir
import MediaDir
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk,  GLib, Gio

from MediaDir import MediaType, MediaFile
import ThreadPool


BOXFILE_TOTAL_HEIGHT = 220
BOXFILE_TOTAL_WIDTH = 235
THUMB_DISPLAY_SIZE = 180
MAX_LABEL_CHARS = 24 
# Change THUMB_DIMENSIONS in MediaDir.py for alternative thumb pixel size.




class FilesListItem(Gtk.FlowBoxChild):

    #FilesBox
    parent = None
    
    overlay:Gtk.Overlay             = None
    container:Gtk.Box               = None
    thumb:Gtk.Image                 = None
    label:Gtk.Label                 = None
    media_file:MediaFile            = None
    thumb_building:bool             = False
    thumb_job:ThreadPool.PoolJob    = None

    def get_media_file(self):
        return self.media_file

    def get_file_name(self):
        return self.media_file.name

    def __init__(self, 
                 file_name:str=None,
                 media_file:MediaFile=None,
                *args, 
                **kwargs):
        super().__init__(*args, **kwargs)

        if media_file:
            self.media_file = media_file

        self.container = Gtk.Box(spacing=6)
        self.container.add_css_class("file-item")
        self.container.set_orientation(Gtk.Orientation.VERTICAL)
        self.set_size_request(BOXFILE_TOTAL_WIDTH, BOXFILE_TOTAL_HEIGHT)

        self.container.set_vexpand(False)
        self.container.set_hexpand(False)

        self.container.set_spacing(8)
        self.container.set_baseline_position(Gtk.BaselinePosition.CENTER)
        self.container.set_margin_top(1)
        self.container.set_margin_bottom(1)
        self.container.set_margin_start(3)
        self.container.set_margin_end(5)

        #self.container.set_name(self.file_name)

        self.label = Gtk.Label()
        self.label.add_css_class("file-item-label")   
        self.label.set_justify(Gtk.Justification.CENTER)
        self.label.set_lines(1)
        self.label.set_single_line_mode(1)
        self.label.set_selectable(True)
        self.label.set_valign(Gtk.Align.CENTER)
        self.label.set_vexpand(False)
        self.label.set_max_width_chars(MAX_LABEL_CHARS)
        self.label.set_ellipsize(True)

        if (file_name):
            self.set_label_text(file_name)


        self.set_child(self.container)

        GLib.idle_add(self.container.append, self.label)
        #self.container.append(self.label)

    def set_thumb(self,
                  thumb_path:str=None,
                  thumb_resource:Gtk.Image=None):

        if thumb_resource:
            self.thumb = thumb_resource
        else:
            self.thumb = Gtk.Image.new_from_file(thumb_path)
        self.thumb.add_css_class("file-thumb")

        self.thumb.set_size_request(THUMB_DISPLAY_SIZE, THUMB_DISPLAY_SIZE)
        self.thumb.set_pixel_size(THUMB_DISPLAY_SIZE)

        self.container.prepend(self.thumb)
        

    def __got_thumb_cb(self, thumb_path:str, params):

        column:FilesListItem    = params["column"]
        file:MediaFile          = params["file"]
        icon                    = None

        if not thumb_path:
            icon_name = "text-x-generic-symbolic"
            match file.get_type():
                case MediaType.IMAGE:
                    icon_name = "image-missing-symbolic"
                case MediaType.AUDIO:
                    icon_name = "audio-x-generic"
                case MediaType.VIDEO:
                    icon_name = "camera-video-symbolic"
                case MediaType.DIR:
                    icon_name = "folder"
                case _:
                    ext = file.get_ext()
                    if ext is None and isdir(file.get_path()):
                        icon_name = "folder"

                    if not icon:
                        mime_guessed = MediaDir.guess_mime_by_ext(ext)
                        
                        if mime_guessed:
                            default_app = Gio.AppInfo.get_default_for_type(mime_guessed, False)
                            if default_app:
                                app_icon = default_app.get_icon()
                                icon = Gtk.Image()
                                Gtk.Image.set_from_gicon(icon, app_icon)

                        if not icon:
                            g_info = column.get_g_info()
                            mime_real = g_info.get_content_type()
                            default_app = Gio.AppInfo.get_default_for_type(mime_real, False)
                            if default_app:
                                app_icon = default_app.get_icon()
                                if app_icon:
                                    icon = Gtk.Image()
                                    Gtk.Image.set_from_gicon(icon, app_icon)
            if not icon:
                icon = Gtk.Image.new_from_icon_name(icon_name)
        
            GLib.idle_add(column.set_thumb, None, icon)
            return
        try:
            GLib.idle_add(column.set_thumb, thumb_path, None)
        except Exception as err:
            pass
    
    def request_thumb(  self,
                        media_dir:MediaDir  = None,
                        file:MediaFile      = None,
                        win                 = None
                    ):
        
        if self.thumb or self.thumb_building:
            return None
        
        self.thumb_building = True

        if not file:
            file = self.media_file

        if not media_dir:
            media_dir=file.parent

        cb_params = {
                "media_dir":    media_dir,
                "column":       self,
                "file":         file
        }
        
        self.thumb_job = win.t_pool.add_job(
                    target=file.get_thumb_file,
                    args=[],
                    cb=self.__got_thumb_cb,
                    cb_params=cb_params,
        )
        return self.thumb_job

    def set_label_text(self, file_name:str=None):
        if file_name:
            self.file_name = file_name 
        displayname = self.file_name
        if len(displayname) > MAX_LABEL_CHARS:
            ext_start = displayname.rfind(".")
            ext = displayname[ext_start:]
            if ext_start == -1 or ext_start < len(displayname) - 5:
                displayname = displayname[:MAX_LABEL_CHARS]
            else:
                displayname = displayname[:MAX_LABEL_CHARS - len(ext) - 1] + "." + ext
        self.label.set_text(displayname)
        self.label.set_tooltip_text(file_name)


    def clear(self):
        if self.thumb:
            self.thumb.clear()
            self.container.remove(self.thumb)
            self.thumb.run_dispose()
            #self.thumb.destroy()
            self.thumb = None
            self.container.remove(self.label)
            
        if self.label:
            self.container.remove(self.label)
            #Gtk.Widget.destroy(self.container)
            #self.label.destroy()
            self.label.run_dispose()
            self.label = None

        self.get_parent().remove(self.container)

        #self.clear()
        #Gtk.FlowBoxChild.clear(self)
        #Gtk.Widget.destroy(self)
        self.run_dispose()


    def get_path(self):
        path = self.get_parent().get_media_dir().path + self.file_name
        return path

    def open_file(self):
        
        file_path = self.get_parent().get_media_dir().path + self.file_name

        if isdir(file_path):
            return self.get_parent().get_window().change_dir(file_path)
        
        g_file = Gio.File.new_for_path(file_path)
        Gio.AppInfo.launch_default_for_uri(g_file.get_uri())


    def get_g_info(self, q="standard::*"):

        media_dir = self.get_parent().get_media_dir()
        f_path = media_dir.path + self.file_name

        # https://stackoverflow.com/a/2170880
        g_file = Gio.File.new_for_path(f_path)

        return g_file.query_info(
            q,
            0,
            None,
        )







