import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Gio

import gc
from time import sleep

def init_animated_thumbs(win):

    return

    win.thumb_streams = []

    def tab_created(args):
        tab = args[2]
        files = tab.files_box

        def file_selected(files_box, win):
            if len(win.thumb_streams):
                clear_old_streams(win.thumb_streams)

            file_child = next(iter(files_box.get_selected_children()[:1]))
            if not file_child:
                return

            media_dir = files_box.get_media_dir()
            f_path = media_dir.path + file_child.file_name

            # https://stackoverflow.com/a/2170880
            g_file = Gio.File.new_for_path(f_path)
            info = g_file.query_info(
                "standard::",
                0,
                None,
            )
            mime = info.get_content_type()
            #throws RuntimeError: This method is currently unsupported.
            #info.unref()

            if str(mime) in ["image/gif", "video/mp4", "video/webm"]:
                #assert(len(win.thumb_streams) == 0)
                win.thumb_streams.append(prepare_animated_thumb(
                    selected_child=file_child,
                    media_dir=media_dir,
                ))


        files.connect(
            "selected-children-changed",
            lambda *args: file_selected(
                files_box=args[0],
                win=win
            )
        )

    win.connect(
            "tab-added", 
            lambda *args: tab_created(args)
    )

    win.connect(
            "page-clearing", 
            lambda *args: clear_old_streams(win.thumb_streams)
    )



def clear_stream(stream):
    if stream.thumb:
        thumb = stream.thumb
        orig_paintable = thumb.paintable_orig
        parent = thumb.get_parent()

        flow_child = parent.get_parent()
        flow_child.container.remove(thumb)

        thumb.clear()
        thumb.run_dispose()

        thumb = None

        new_thumb = Gtk.Image.new_from_paintable(orig_paintable)
        #orig_paintable = None

        flow_child.set_thumb(
            thumb_resource=new_thumb
        )

    file = stream.get_file()
    file.run_dispose()
    
    Gtk.MediaFile.run_dispose(stream)
    #thumb.stream.invalidate_contents()
    stream.do_close(stream)
    stream.clear()
    stream.run_dispose()

    stream.thumb = None
    stream = None


def clear_old_streams(thumb_streams:list[Gtk.Image]):
    print("Siivoaan vanha")
    for stream in thumb_streams:
        clear_stream(stream=stream)
        #raise Exception("??")

    thumb_streams.clear()

def stream_prepared_cb(*args):
    _stream = args[0]
    _stream.play()
    _thumb = _stream.thumb
    _thumb.set_from_paintable(_stream)

def prepare_animated_thumb(selected_child, media_dir):

    # Gtk.Image
    thumb = selected_child.thumb
    if not thumb.is_visible():
        return
    f_path = media_dir.path + selected_child.file_name

    """
    # mp4 file
    f_path = media_dir.path + selected_child.file_name
    #g_file = Gio.File.new_for_path(f_path)

    media_file = Gtk.MediaFile.new()

    thumb.set_from_paintable(media_file)
    media_file.parent = thumb

    media_file.connect("notify::prepared", prepared)
    #media_file.set_file(g_file)
    media_file.set_resource(f_path)
    media_file.play()

    return
    """

    thumb.paintable_orig = thumb.get_paintable()

    # Requires libgtk-4-media-gstreamer.
    stream = Gtk.MediaFile.new()
    stream.thumb = thumb

    stream.connect("notify", lambda *args: print(args))
    stream.connect("notify::prepared", stream_prepared_cb)

    g_file = Gio.File.new_for_path(f_path)
    # Doesn't work?
    #thumb.stream.set_resource(f_path)
    stream.set_file(g_file)

    #thumb.stream.play()

    return stream

def prepared(*args):
    #Gtk.MediaFile
    media_file = args[0]
    #Gtk.Image
    img = media_file.parent
    #Gtk.Box
    container = img.get_parent()
    media_file.clear()
    img.clear()
    container.remove(img)
    #media_file = None