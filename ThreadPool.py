from threading import Condition, Lock, Thread

from  multiprocessing import cpu_count

from typing import NamedTuple
from typing import Tuple
from typing import List
#import queue
from collections import deque  

from typing import NamedTuple

class PoolJob(NamedTuple):
    target:     callable
    args:       list
    done_cb:    callable
    cb_params:  any


class ThreadPool:
    #jobs:List[PoolJob] = []
    queue = deque([])
    running_jobs:List[PoolJob] = []
    pool_size: int = 1
    threads = []
    cv = Condition()
    job_done_cv = Condition()
    lock = Lock()
    #lock.release()
    keep_running = True

    def __init__(self, pool_size=None, *args, **kwargs):
        if pool_size:
            self.pool_size = pool_size
        else:
            self.pool_size = cpu_count()

    def is_busy(self):
        return len(self.queue) or len(self.running_jobs)
        #return len(self.jobs) or len(self.running_jobs)

    def remove_job(self, job:PoolJob):
        with self.cv:
            self.cv.acquire()
            if job in self.queue:
                self.queue.remove(job)
            self.cv.release()


    def clear_jobs(self, cb:callable=None, cb_param=None):
        #self.jobs.clear() 
        self.queue.clear()
        if not cb:
            return
        with self.job_done_cv:
            while len(self.running_jobs):
                self.job_done_cv.wait()
        if cb:
            cb(cb_param)
        return

    def wait_tasks_to_finish(self):
        with self.job_done_cv:
            while self.is_busy():
            #while len(self.jobs) or len(self.running_jobs):
                self.job_done_cv.wait()

    def add_job(self, target:callable, args, cb=None, cb_params=None):
        with self.cv:
            self.cv.acquire()
            job = PoolJob(
                target=target,
                args=args,
                done_cb=cb,
                cb_params=cb_params
            )
            self.queue.append(job)
            self.cv.notify()
            self.cv.release()

            return job

    def run_worker(self):
        while self.keep_running:
            job = None
            
            with self.cv:
                while not len(self.queue):
                    self.cv.wait()
                self.cv.acquire()
                job = self.queue.popleft()

            self.running_jobs.append(job)
            self.cv.release()
            target = job.target
            res = target(*job.args)
            self.running_jobs.remove(job)
            if job.done_cb:
                job.done_cb(res, job.cb_params)

            self.job_done_cv.acquire()
            self.job_done_cv.notify()
            self.job_done_cv.release()

    def start_workers(self):
        print("Staring threads")
        for i in range(0, self.pool_size):
            t = Thread(target=self.run_worker, args=[], daemon=True)
            t.start()
            self.threads.append(t)
            print("..started thread " + str(i))
        print("...started all threads")

    def stop(self):
        self.keep_running = False
