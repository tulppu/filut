import gi

from GtkDialogs import show_error
import GtkDialogs
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Gdk, GLib, Gio


def trash_selected_files(controller, keyval, keycode, state, win):

    if keyval != Gdk.KEY_Delete:
        return
    
    selected_childs_all = win.get_current_tab().files_box.get_selected_children()

    confirmation_dialog = Gtk.AlertDialog()

    files_list_string = " ".join(map(lambda f: "- {}\n".format(f.get_path()), selected_childs_all))

    confirmation_dialog.set_message("Poista seuraavat {} kohdetta?\n\n{}".format(len(selected_childs_all), files_list_string))

    confirmation_dialog.set_buttons(["_Cancel", "_Delete"])
    confirmation_dialog.set_cancel_button(0)
    confirmation_dialog.set_default_button(0)

    def modal_closed(dialog, result):
        response = dialog.choose_finish(result)
        user_cancelled = response == dialog.get_cancel_button()
        if user_cancelled:
            return


        def _on_file_trashed(file, result, user_data):
            try:
                file.trash_finish(result)
                print(f"{file.get_uri()} poistettu.")
            except GLib.Error as e:
                # TODO: use some global shared helper function for simple dialog without choices. 
                print(f"Failed to trash {file.get_uri()}: {e.message}")
                GtkDialogs.show_error(e.message, win)
                # error_dialog = Gtk.AlertDialog()
                # error_dialog.set_message(e.message)
                # error_dialog.set_buttons(["OK"])
                # error_dialog.show(win)

        for f in selected_childs_all:
            gfile = Gio.File.new_for_path(f.get_path())
            gfile.trash_async(GLib.PRIORITY_DEFAULT, None, _on_file_trashed, None)


    confirmation_dialog.choose(win, None, modal_closed)

def focus_location(controller, keyval, keycode, state, win):
    if keyval != Gdk.KEY_l or not (state & Gdk.ModifierType.CONTROL_MASK):
        return
    win.header.path.grab_focus()

def init_accels(win: Gtk.Window):

    keycont = Gtk.EventControllerKey()
    keycont.connect("key-pressed", trash_selected_files, win)
    keycont.connect("key-pressed", focus_location, win)
    win.add_controller(keycont)


