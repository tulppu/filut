
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk 

class ButtonsRow(Gtk.Box):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_css_class("buttons-row")
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.set_valign(Gtk.Align.CENTER)
        self.set_halign(Gtk.Align.CENTER)
        self.set_margin_top(5)
        self.set_margin_bottom(5)

    def add_button(self, label_text:str=None, icon_name:str=None) -> Gtk.Button:
        button = Gtk.Button.new()
        self.append(button)
        button.set_margin_start(3)
        button.set_margin_end(3)

        if label_text:
            button.set_label(label_text)

        if icon_name:
            button.set_icon_name(icon_name)

        return button
