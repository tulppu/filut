from typing import List
from os.path import isdir
from os import fork, popen, spawnl, P_NOWAIT


import gi

import GtkDialogs
import GtkFileListItem
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gdk, GLib, GObject, Gio, Pango

app = None
get_ctx_menu = None


def get_current_tab():
    global app
    return  app.get_active_window().get_current_tab()


def execute_convert(action:Gio.SimpleAction, target_ext:GLib.Variant):
    from subprocess import Popen, PIPE, STDOUT, CalledProcessError, TimeoutExpired
    # from FileList import FilesBoxChild

    dialog = Gtk.Window()
    dialog.set_modal(True)
    dialog.set_title("Converting..")

    container = Gtk.Box()
    container.set_orientation(Gtk.Orientation.VERTICAL)
    container.set_size_request(250, 50)

    dialog.set_child(container)

    prog_bar = Gtk.ProgressBar.new()
    prog_bar.set_show_text(True)
    prog_bar.set_pulse_step(0.1)
    container.append(prog_bar)
    dialog.present()

    curr_tab = get_current_tab()
    selected_file:GtkFileListItem.FilesListItem = curr_tab.files_box.get_selected_children()[0]
    input_path = selected_file.media_file.get_path() 
    output_path = input_path + target_ext.get_string()

    def _start(): 
        def _exec():
            
            in_ext = input_path[input_path.rfind("."):]
            out_ext = output_path[output_path.rfind("."):]
            
            cmd = ["ffmpeg", "-y", "-loglevel", "quiet", "-stats", "-i", input_path, output_path]

            if (in_ext == ".flac" and out_ext == ".opus"):
                #in case shutil not installed.
                try:
                    from shutil import which
                    if which("opusenc"):
                        cmd = ["opusenc", "--bitrate", "128", input_path, output_path]
                except Exception as err:
                    pass

            proc = Popen(cmd, stdout=PIPE, stderr=STDOUT, universal_newlines=True, bufsize=1, text=True)

            def _close(*args):
                proc.terminate()

            dialog.connect("close-request", _close)

            while True:
                try:
                    proc.wait(timeout=1)
                    if proc.returncode is not None:
                        print("Compress done")
                        break 
                except TimeoutExpired:
                    if proc.returncode is not None:
                        print("ffmpeg returned" + str(proc.returncode))
                line = proc.stdout.readline()
                yield line

            proc.terminate()
        
        for _line in _exec():
            GLib.idle_add(prog_bar.pulse)
            GLib.idle_add(prog_bar.set_text, "\n" + _line)

    def _compress_done(*args):
        prog_bar.set_text("\nCompleted.\n")
        dialog.set_title("Converted.")
        from Gtk import ButtonsRow
        buttons = ButtonsRow()

        def select_compressed_file(*args):
            new_item = curr_tab.files_box.get_child_by_path(output_path)
            if new_item:
                curr_tab.files_box.select_child(new_item)
                new_item.grab_focus()

        btn_select = buttons.add_button(label_text="Valitse")
        btn_select.connect("clicked", select_compressed_file)

        def _copy_path(args):
            gfile = Gio.File.new_for_path(output_path)
            _path = GObject.Value(GObject.TYPE_STRING)
            _path.set_string(gfile.get_uri())
            Gdk.Display().get_default().get_clipboard().set_content(
                Gdk.ContentProvider.new_for_value(_path)
            )

        btn_copy_path = buttons.add_button(label_text="Kopioi polku")
        btn_copy_path.connect("clicked", _copy_path)

        btn_ok = buttons.add_button(label_text="Ok")
        btn_ok.connect("clicked", lambda *args: dialog.close())


        container.append(buttons)
        #dialog.close()

    from ThreadPool import ThreadPool
    worker:ThreadPool = app.win.t_pool
    worker.add_job(
        target=_start,
        args=[],
        cb=lambda *args: GLib.idle_add(_compress_done)
    )

    prog_bar.pulse()




def execute_open_with(action:Gio.SimpleAction, app_id_param:GLib.Variant):
    app_id = app_id_param.get_string()
    apps_all = Gio.AppInfo.get_all()
    app_selected = next((app for app in apps_all if app.get_id() == app_id), None)

    curr_tab = get_current_tab()
    curr_media_dir = curr_tab.media_dir

    selected_files = curr_tab.files_box.get_selected_children()
    file_paths = list(map(lambda item: curr_media_dir.path + item.file_name, selected_files))
    gfiles = list(map(lambda path: Gio.File.new_for_path(path), file_paths))

    app_selected.launch(gfiles)

def execute_copy(action:Gio.SimpleAction, param:GLib.Variant):
    curr_tab = get_current_tab()
    curr_media_dir = curr_tab.media_dir
    selected_files = curr_tab.files_box.get_selected_children()

    global _win

def execute_copy_path(action:Gio.SimpleAction, param:GLib.Variant):
    curr_tab = get_current_tab()
    curr_media_dir = curr_tab.media_dir

    #global context_menu
    context_menu = get_ctx_menu()
    path_str = curr_media_dir.path + context_menu.popped_item.file_name

    gfile = Gio.File.new_for_path(path_str)

    path = GObject.Value(GObject.TYPE_STRING)
    path.set_string(gfile.get_uri())

    Gdk.Display().get_default().get_clipboard().set_content(
        Gdk.ContentProvider.new_for_value(path)
    )

def execute_copy_name(action:Gio.SimpleAction, param:GLib.Variant):
    #global context_menu
    context_menu = get_ctx_menu()
    name_str = context_menu.popped_item.file_name

    Gdk.Display().get_default().get_clipboard().set_content(
        Gdk.ContentProvider.new_for_value(name_str)
    )


def execute_trash(action:Gio.SimpleAction, param:GLib.Variant):
    curr_tab = get_current_tab()
    context_menu = get_ctx_menu()
    path_str =  context_menu.popped_item.get_path()

    flow_item = context_menu.popped_item
    _next =  flow_item.get_next_sibling() or flow_item.get_prev_sibling()
    _next.get_parent().select_child(_next)

    gfile = Gio.File.new_for_path(path_str)
    gfile.trash_async(1, None, None, None)

def execute_rename(action:Gio.SimpleAction, param:GLib.Variant):
    curr_tab = get_current_tab()
    context_menu = get_ctx_menu()
    path_str =  context_menu.popped_item.get_path()

    gfile = Gio.File.new_for_path(path_str)

    def dialog_closed(resp, new_filename, _dialog):
        if resp == Gtk.ResponseType.CANCEL:
            _dialog.close()
            return

        if  not len(new_filename):
            return

        try: 
            gfile.set_display_name(new_filename)
            context_menu.popped_item.set_label_text(new_filename)
            _dialog.close()
        except Exception as err:
            _dialog.show_message(str(err))
            print(err)
    
    namedialog = GtkDialogs.InputDialog(curr_tab.win, "Uudelleennimeä", gfile.get_basename(), dialog_closed)
    namedialog.set_input(gfile.get_basename())
    namedialog.present()


def init(_app, get_ctx_menu_func):
    global app
    app = _app
    global get_ctx_menu
    get_ctx_menu = get_ctx_menu_func
