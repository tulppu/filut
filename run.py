#!/usr/bin/env python3


import gi

import GtkDialogs
import GtkFileList
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, GLib, GObject, Gio

import os
import re
import time
from threading import Lock
from typing import List
from os.path import isdir


from MediaDir import MediaFile, MediaDir, MediaSorting
import ThreadPool;
import Lazyload;

import GtkFileListItem
import GtkHeader 
import GtkFooter
import GtkPanel
import GtkAccels

# import AnimatedThumbs
from GtkContextMenu import init_context_menu_actions

from  multiprocessing import cpu_count

FILES_ADDED_PER_BATCH = 32 


class MainWindow(Adw.ApplicationWindow):
    page_lock = Lock()
    lock_new_items = Lock()

    #main_box = Adw.Leaflet()
    main_box = Gtk.Box()
    main_box.set_orientation(Gtk.Orientation.VERTICAL)

    # Should be used only for directory/tab related tasks like generating thumbs 
    # or building file columns without blocking the main loop.
    # Is emptied if tab paginated page is changed.
    t_pool = ThreadPool.ThreadPool(pool_size=cpu_count()-1)
    t_pool.start_workers()

    tab_loader = ThreadPool.ThreadPool(pool_size=1)
    tab_loader.start_workers()

    gesture_window = Gtk.GestureClick.new()
    gesture_files = Gtk.GestureClick.new()

    tab_bar:Adw.TabBar = Adw.TabBar().new()
    tab_view:Adw.TabView = Adw.TabView.new()

    tab_bar.add_css_class("tab-bar")
    tab_view.add_css_class("tab-view")
    
    #tab_view.end_widget = Gtk.Box(Gtk.Orientation.HORIZONTAL, 5)
    tab_bar.end_button = Gtk.Button.new_from_icon_name("tab-new-symbolic")
    #tab_bar.end_button.connect("clicked", lambda *args: )
    tab_bar.set_end_action_widget(tab_bar.end_button)

    paned:Gtk.Paned = None

    signals = {}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
       
        self.tab_bar.end_button.connect("clicked", lambda *args: self.create_tab(parent=self.get_current_tab()))

        self.register_emitter()

        self.set_content(self.main_box)

        self.tab_bar.set_view(self.tab_view)
        self.tab_bar.set_autohide(False)

        self.paned = Gtk.Paned.new(orientation=Gtk.Orientation.HORIZONTAL)
        self.paned.set_end_child(self.tab_view)
        
        self.main_box.append(self.paned)

        self.tab_view.set_vexpand(True)
        self.tab_view.set_hexpand(True)
        

        GtkHeader.init_header(self)
        GtkFooter.init_footer(self) 
        GtkPanel.init_panel(self)

        Lazyload.init_lazyload(self)
        GtkAccels.init_accels(self)
        #AnimatedThumbs.init_animated_thumbs(self)
    
        init_context_menu_actions(self)


        def tab_added_cb(args):
            tab = args[2]
            if tab == self.get_current_tab():
                self.display_page(0, tab)

        self.connect(
                "tab-added", 
                lambda *args: tab_added_cb(args)
        )

        self.create_tab(dir_path=GLib.get_home_dir())
        self.create_tab(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PICTURES))
        self.create_tab(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS))

        def tab_notified(_view:Adw.TabView, g_param):
            page = _view.get_selected_page()
            if page.current_page == -1:
                self.display_page()    

        self.tab_view.connect("notify::selected-page", tab_notified) 


    def register_emitter(self):

        def _register(event_name:str, args):
            self.signals[event_name] = GObject.signal_new(
                event_name,
                self,
                GObject.SignalFlags.RUN_LAST,
                bool,
                args
            )

        _register("page-changed", (int, object))
        _register("page-clearing", (int, object))
        _register("tab-added", (int, object))
        _register("childs-added", (int, object, object))

    def emit_page_clearing(self, tab):
        self.emit("page-clearing", 1, tab)

    def emit_page_change(self, tab):
        self.emit("page-changed", 1, tab)

    def emit_tab_added(self, tab:Adw.TabPage):
        self.emit("tab-added", 1, tab)

    def emit_childs_added(self, tab:Adw.TabPage, childs:GtkFileListItem.FilesListItem):
        self.emit("childs-added", 1, tab, childs)


    def create_tab(self,
                   dir_path=None,
                   media_dir=None,
                   parent=None):

        if not dir_path:
            dir_path = GLib.get_home_dir()

        if not isdir(dir_path):
            raise(f"Invalid dir '{dir_path}'")

        if not dir_path.endswith("/"):
            dir_path = dir_path + "/"

        files_box = GtkFileList.FileList()
        files_box.window = self 
        tab_page = None

        #if not parent:
        #    parent = self.get_current_page()

        if parent:
            tab_page = self.tab_view.add_page(
                files_box.overlay,
                parent
            )
        else:
            tab_page = self.tab_view.append(files_box.overlay)

        tab_page.files_box = files_box


        tab_page.win = self
        tab_page.window = self
        tab_page.set_title(dir_path)
        tab_page.current_page = -1
        tab_page.q = None
        tab_page.set_tooltip(dir_path)
        #tab_page.set_vexpand(False)
        #tab_page.set_hexpand(False)


        files_box.tab = tab_page

        if not media_dir:
            def dir_loaded(*args):
                tab_page.media_dir = args[0][0]
                GLib.idle_add(self.emit_tab_added, tab_page)

            self.tab_loader.add_job(
                target=self.set_media_dir,
                args=[
                    dir_path,
                    tab_page,
                ],
                cb=lambda *args: dir_loaded(args)
            )
        else:
            tab_page.media_dir = media_dir
            self.emit_tab_added(tab_page)

        return tab_page
            
    def get_current_tab(self):
        return self.tab_view.get_selected_page()
    
    def get_media_dir(self):
        tab = self.get_current_tab()
        if not tab or not hasattr(tab, "media_dir"):
            return None
        return tab.media_dir

    def get_current_page(self):
        return self.get_current_tab().current_page

    def clear_tab(self, tab=None, cb=None):
        self.emit_page_clearing(tab)
        tab = tab if tab else self.get_current_tab()
        if not tab:
            return
        tab.files_box.clear(cb)
        # tab.media_dir.uninstall_monitor()


    def change_sorting(self, sort:MediaSorting=None):
        print("Changing sort")

        tab = self.get_current_tab()

        if not sort:
            sort = tab.media_dir.sorting

        tab.media_dir.sorting = sort

        def sort_done(tab):
            tab.win.clear_tab(tab)
            tab.win.display_page(0, tab=tab)

        tab.media_dir.sort(
            sorting=tab.media_dir.sorting,
            worker=self.tab_loader,
            #cb=sort_done,
            cb=lambda *args: GLib.idle_add(sort_done, args[1]),
            #cb=lambda *args: print(args[1]),
            cb_params=tab,
        )

    def toggle_display_hiddens(self, display:bool=False):
        tab = self.get_current_tab()
        tab.media_dir.display_hiddens = display
        tab.media_dir.dirty = True
        self.refresh_current_dir(hard=False)

    def toggle_reverse_order(self, reverse_sort:bool=False):
        tab = self.get_current_tab()
        tab.media_dir.sort_reversed = reverse_sort
        tab.media_dir.dirty = True
        self.change_sorting()
        #self.refresh_current_dir(hard=False)


    def change_to_page(self, page:int=0):
        self.clear_tab()
        self.display_page(page=page)

    def set_media_dir_async(self, path, tab_page, cb_done):

        tab_page.set_loading(True)

        def _dir_loaded(res, *args):
            tab_page.set_loading(False)
            cb_done(res)

        self.t_pool.add_job(
            target=self.set_media_dir,
            args=[path, tab_page],
            cb=lambda res, *args: GLib.idle_add(_dir_loaded, res) 
        )

    def set_media_dir(self, path, tab_page, cb=None):

        media_dir_old = self.get_media_dir()
        if media_dir_old:
            media_dir_old.uninstall_monitor()

        media_dir = None

        try:
            media_dir = MediaDir.load_from_path(
                dir_path=path,
                #sorting=tab.sorting
            )
        except Exception as err:
            GtkDialogs.show_error(str(err), self)
            return None


        def cb_file_added(m_file:MediaFile):
            new_column:GtkFileListItem.FilesListItem = tab_page.win.build_file_column(m_file)
            tab_page.files_box.append(new_column)

        def cb_file_removed(m_file:MediaFile):
            def remove_child(child:GtkFileListItem.FilesListItem):
                child.clear()

            iter:GtkFileListItem.FilesListItem = tab_page.files_box.get_child_at_index(0)
            while iter:
                if iter.media_file is m_file:
                    next_to_focus =  iter.get_next_sibling() or iter.get_prev_sibling()
                    if next_to_focus:
                        next_to_focus.get_parent().select_child(next_to_focus)
                    GLib.idle_add(remove_child, iter)
                    break
                iter = iter.get_next_sibling()

        media_dir.install_monitor(
            callback_file_created=lambda m_file: GLib.idle_add(cb_file_added, m_file),
            callback_file_removed=lambda m_file: GLib.idle_add(cb_file_removed, m_file),
        )

        return media_dir


    def change_dir(self, path:str|None=None):
        path = path if path else self.header.path.get_buffer().get_text()

        if not os.path.isdir(path):
            GtkDialogs.show_error(f"Virheellinen polku '{path}'", self)
            raise Exception(f"Invalid path '{path}'")
        
        if not path.endswith("/"):
            path = path + "/"

        tab = self.get_current_tab()

        display_hiddens_old = tab.media_dir.display_hiddens
        sort_reversed_old = tab.media_dir.sort_reversed

        print("Changing to " + path)
        self.clear_tab()

        def dir_loaded(media_dir):
            if media_dir:
                media_dir.display_hiddens = display_hiddens_old
                media_dir.sort_reversed = sort_reversed_old

                tab.media_dir = media_dir 
            tab.set_title(path)

        self.set_media_dir_async(path=path, tab_page=tab, cb_done=dir_loaded)

        self.display_page(page=0)

    def refresh_current_dir(self, hard=True):
        if hard:
            self.change_dir(self.get_current_tab().media_dir.path)
        else:
            self.get_current_tab().files_box.clear()
            self.display_page(0)

    def build_file_column(self, file:MediaFile) -> GtkFileListItem.FilesListItem:
            file_column = GtkFileListItem.FilesListItem(
                file_name=file.name,
                media_file=file,
            )
            return file_column
        
    def append_file_batch(self,
                          columns:GtkFileListItem.FilesListItem,
                          tab=None
                        ):
        tab.files_box.append_many(columns)
        self.emit_childs_added(childs=columns, tab=tab)


    def display_items(self, items, tab=None):

        def process_batch(batch:List[MediaFile]):
            built_items = list(map(self.build_file_column, batch))
            if self.lock_new_items.locked():
                return
            self.append_file_batch(built_items, tab)

        while items:
            if self.lock_new_items.locked():
                #print("rikotaan")
                break
            sliced_items = items[:FILES_ADDED_PER_BATCH]
            GLib.idle_add(process_batch, sliced_items)
            items = items[FILES_ADDED_PER_BATCH:]
            # Give some time for main the main loop
            time.sleep(0.05)
            #GLib.MainContext.iteration(None, True)
            continue 

        if self.page_lock.locked():
            self.page_lock.release()

    def display_page(self, page:int=0, tab=None):
  
        tab = tab if tab else self.get_current_tab()

        def jobs_cleared(_cb_params):
            _cb_params["lock"].release()
            GLib.idle_add(
                self.display_page,
                _cb_params["page"],
                 _cb_params["tab"]
            )


        if self.lock_new_items.locked():
            return

        #print("Näytetään sivu")

        if self.t_pool.is_busy():
            self.lock_new_items.acquire()
            self.clear_tab(tab)
            GLib.idle_add(
                self.t_pool.clear_jobs,
                jobs_cleared,
                {
                    "page": page,
                    "tab": tab,
                    "lock": self.lock_new_items
                }
            )
            return

        #q = self.header.filter.get_buffer().get_text()
        q = self.get_current_tab().files_box.get_filter_string().lower()

        re_pattern = None
        if len(q) >= 3 and q[0] == '/' and q[-1] == '/':
            try:
                re_pattern = re.compile(q[1:-1], re.IGNORECASE)
                q = None
            except Exception as err:
                print("Failed to compile filtering regex")
                print(err) 

        media_dir = tab.media_dir
        items_displayed = media_dir.get_items(page, q, re_pattern)

        tab.current_page = page

        self.t_pool.add_job(
            target=self.display_items,
            args=[items_displayed, tab],
            cb=lambda *args: print("Sivu valmis")
        )

        self.emit_page_change(tab)

def add_css(display, style_path:str=None):
        if not style_path:
            import os 
            style_path = os.path.dirname(os.path.realpath(__file__)) + "/latu.css"

        css_file = Gio.File.new_for_path(style_path)
        css_provider = Gtk.CssProvider.new()

        Gtk.CssProvider.load_from_file(css_provider, css_file) 
        Gtk.StyleContext.add_provider_for_display(display, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)

class HUippu(Adw.Application):
    
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        add_css(self.win.get_display()) 
        self.win.set_title("HUippu")
        self.win.present()
        

app = HUippu(application_id="com.latu.GtkApplication")
app.run(None)

loop = GLib.MainLoop()
loop.run()
