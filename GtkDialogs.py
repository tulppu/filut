import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Gdk, Adw, GLib, Gio

class InputDialog(Gtk.Window):
    def __init__(self, parent, title: str, placeholder: str, result_callback):
        def _send_signal_back(resp):
            result_callback(resp, self.entry.get_buffer().get_text(), self)
            # self.close()

        def on_cancel(self):
            _send_signal_back(Gtk.ResponseType.CANCEL)

        def on_ok(self):
            # print(f"User input: {self.entry.get_text()}")
            _send_signal_back(Gtk.ResponseType.OK)

        super().__init__(title=title, transient_for=parent)
        self.set_default_size(300, 50)
        self.set_modal(True)

        # Pääkontaineri
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox.set_margin_top(10)
        vbox.set_margin_bottom(10)
        vbox.set_margin_start(10)
        vbox.set_margin_end(10)
        self.set_child(vbox)

        # Tekstikenttä
        self.entry = Gtk.Entry()
        self.entry.set_placeholder_text(placeholder)
        self.entry.connect("activate", lambda *args: on_ok(self))
        vbox.append(self.entry)

        # Painikkeet
        button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        button_box.set_halign(Gtk.Align.CENTER)

        cancel_button = Gtk.Button(label="Cancel")
        ok_button = Gtk.Button(label="OK")

        cancel_button.connect("clicked", on_cancel)
        ok_button.connect("clicked", on_ok)


        button_box.append(cancel_button)
        button_box.append(ok_button)

        vbox.append(button_box)

        # Viestikenttä
        self.message = Gtk.Label()
        self.message.set_visible(False)
        vbox.append(self.message)

        # Esc
        def _key_listener(controller, keyval, keycode, state):
            if keyval == Gdk.KEY_Escape:
                on_cancel(self)
                return True
            return False

        key_controller = Gtk.EventControllerKey()
        key_controller.connect("key-pressed", _key_listener)
        self.add_controller(key_controller)

    def set_input(self, inp):
        self.entry.get_buffer().set_text(inp, len(inp))

        
    def show_message(self, message):
        self.message.set_visible(True)
        self.message.set_text(message)

    def clear_message(self):
        self.message.set_visible(False)
        self.message.set_text(None)



def show_error(message, win):

    error_dialog = Gtk.AlertDialog()
    error_dialog.set_message(message)
    error_dialog.set_buttons(["_OK"])
    error_dialog.show(win)
