import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk

import time

from GtkFileListItem import FilesListItem

def init_lazyload(win):

    def load_n_thumbs_from(n_to_load:int, child_first, files_box):
        iter:FilesListItem = child_first
        for i in range(0, n_to_load):

            if not iter:
                break

            #if iter.thumb_building:
            #    continue

            if len(win.t_pool.queue) > 30:
                last_job = win.t_pool.queue.popleft()
                if "column" in last_job.cb_params:
                    skipped_file:FilesListItem = last_job.cb_params["column"]
                    skipped_file.thumb_building = False
                    skipped_file.thumb = None
                    win.t_pool.remove_job(skipped_file.thumb_job)

            iter.request_thumb(
                media_dir=files_box.tab.media_dir,
                win=files_box.tab.win,
            )
            iter = iter.get_next_sibling()


    def scrolled(files):
        start_time = time.time()
        vadj = files.container.get_vadjustment()
        hadj = files.container.get_hadjustment()
        
        height = vadj.get_page_size()
        pos_y = vadj.get_value()

        width = hadj.get_page_size()
        #size_x = hadj.get_value()

        # Should be not possible to be None unless weird margin or padding added?
        child_first = files.get_child_at_pos(0, pos_y)
        # Can be None

        images_per_row = width / 200
        rows_shown = height / 200

        # Load some in advance
        images_to_load_count = int(images_per_row * rows_shown * 2.5)

        if child_first:
            load_n_thumbs_from(n_to_load=images_to_load_count, child_first=child_first, files_box=files)
        #print("--- %s seconds ---" % (time.time() - start_time))

    win.connect(
            "page-changed", 
            lambda *args: scrolled(args[2].files_box)
    )


    def tab_created(args):

        tab = args[2]
        files = tab.files_box
        scroll_controller = Gtk.EventControllerScroll.new(
            Gtk.EventControllerScrollFlags.BOTH_AXES 
            | Gtk.EventControllerScrollFlags.KINETIC,
        )

        files.container.add_controller(scroll_controller)

        adj = Gtk.Adjustment()
        adj.connect("value-changed", lambda *args: scrolled(files))
        files.container.set_vadjustment(adj)
        
        child_first = files.get_child_at_index(0)
        if not child_first:
            return

        load_n_thumbs_from(40, child_first=child_first, files_box=files)

    def init_load(tab):
        if not hasattr(tab, "files_box"):
            return
        files = tab.files_box

        child_first = files.get_child_at_index(0)
        if not child_first:
            return
        
        if child_first.thumb:
            return

        load_n_thumbs_from(40, child_first=child_first, files_box=files)


    win.connect(
        "tab-added", 
        lambda *args: tab_created(args)
    )

    win.tab_view.connect(
        "notify::selected-page",
        lambda *args: args[0].get_selected_page()
    )

    win.connect(
        "childs-added",
        lambda *args: init_load(args[2])
    )

