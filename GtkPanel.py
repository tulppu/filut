import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, GLib, Gio, Pango, Gdk

import os
import Bookmarks
from subprocess import check_output

class ListBoxDirs(Gtk.ListBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_vexpand(False)
        self.set_hexpand(True)
        self.set_show_separators(False)
        #self.set_size_request(150, -1)
        self.connect("row-activated", self.row_selected)
        self.set_size_request(100, -1)

    def row_selected(self, *args):
        selected = self.get_selected_row()
        if selected and isinstance(selected, ListBoxItem):
            selected.cb_selected(args)

    def add_separator(self):
        sep = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
        sep.set_focusable(False)
        sep.set_focus_on_click(False)
        self.append(sep)
        #sep.set_parent(self)

        return sep

class ListBoxItem(Gtk.ListBoxRow):

    path:str = None
    container:Gtk.Box = None
    icon_name_default = "folder-symbolic"

    def __init__(self, icon_name=None, icon=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if icon_name:
            self.icon_name_default = icon_name
        if icon:
            self.add_icon(icon)

        self.add_css_class("panel-item")

        self.set_margin_top(2)
        self.set_margin_start(5)
        self.set_margin_end(2)
        self.container = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.container.icon = None
        #self.container.set_size_request(64, -1)
        self.container.label = Gtk.Label() 
        #self.container.label.set_size_request(64, -1)
        #self.container.label.set_xalign(Gtk.Align.START)
        self.container.label.set_lines(1)
        self.container.label.set_ellipsize(Pango.EllipsizeMode.END)
        
        self.set_child(self.container)

        if not self.container.icon:
            self.add_icon_by_name(self.icon_name_default)

        separator = Gtk.Separator.new(Gtk.Orientation.VERTICAL)
        separator.set_margin_start(5)
        separator.set_margin_end(5)

        self.container.append(separator)
        self.container.append(self.container.label)

    def cb_selected(self, *args):
        print("valittiin " + self.container.label.get_text())

    def set_label(self, text:str):

        self.container.label.set_text(text)
        self.container.label.set_tooltip_text(text)

    def add_icon(self, icon:Gtk.Image):
        self.container.append(icon)
        self.container.icon = icon
        icon.add_css_class("panel-item-icon")
    
    def add_icon_by_name(self, icon_name:str):
        icon = Gtk.Image.new_from_icon_name(icon_name)
        self.add_icon(icon)


    def get_path(self):
        return self.path
    
    def set_path(self, path:str):
        self.path = path

class ListBoxRowDir(ListBoxItem):
    def __init__(self, gfile=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if gfile:
            self.set_file(gfile)

    def set_file(self, gdir:Gio.File):
        self.set_path(gdir.get_path())
        self.set_label(gdir.get_basename())

class ListBoxRowBookmark(ListBoxRowDir):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ListBoxRowVolume(ListBoxRowDir):

    # ListBoxDrive
    parent_drive_row:any = None
    # Gio.Volume
    gvolume:any = None

    def __init__(self, parent_drive_row, volume, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent_drive_row=parent_drive_row
        self.gvolume = volume
        
        if volume.get_mount() and volume.get_mount().get_root():
            self.set_path(volume.get_mount().get_root().get_path())

        self.set_label(volume.get_name())
        self.update_mount()

    def update_mount(self):
        if (self.gvolume.get_mount()):
            path = self.gvolume.get_mount().get_root().get_path()
            self.set_tooltip_text(path)
            self.set_path(path)
            self.add_css_class("mounted")
            self.remove_css_class("unmounted")
        else:
            self.remove_css_class("mounted")
            self.add_css_class("unmounted")
            print(self.gvolume.get_name() + " is not mounted")


    def set_label(self, volume_name:str):
        cmd = ["df", self.get_path(), "--output=pcent"]
        if self.get_path():
            use_level = check_output(cmd, text=True).split()[1]
            if use_level:
                text = "{} ({})".format(volume_name, use_level)
                return super().set_label(text)
        return super().set_label(volume_name)

    def cb_selected(self, *args):
        _selected = super().cb_selected
        if self.gvolume.get_mount() is None:
            def volume_mounted_cb(*_args_dismimissed):
                mount = self.gvolume.get_mount()
                if not mount:
                    print("Failed to mount " + self.gdrive.get_path())
                    return
                print("Mounted " + mount.get_name())
                self.update_mount()
                _selected(*args)
                #TODO: trigger row-selected or something??

            self.gvolume.mount(Gio.MountMountFlags.NONE, None, None, volume_mounted_cb)
            return

        return _selected(*args)


class ListBoxRowDrive(ListBoxRowDir):

    gdrive:Gio.Drive = None
    icon_name_default="drive-harddisk-symbolic"
    childs:list[ListBoxRowVolume] = []
    separator = None
    parent:Gtk.ListBox = None

    def __init__(self, gdrive, separator, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.separator = separator
        self.set_drive(gdrive)

    def cb_selected(self, *args):
        if self.gdrive and self.gdrive.can_start():

            def drive_started_cb(*args_dismissed):
                print("Started drive " + self.gdrive.get_name())
                self.list_volumes()

            print("Starging drive..?")
            self.gdrive.start(
                Gio.DriveStartFlags.NONE,
                None,
                None,
                drive_started_cb,
                None
            )
        super().cb_selected(args)

    def list_volumes(self):
        volumes = self.gdrive.get_volumes()

        #list_box = self.separator.get_parent().get_parent()
        self.parent = self.separator.get_parent().get_parent()


        for v in reversed(volumes):
            if [i for i in self.childs if i.gvolume and i.gvolume.get_uuid() == v.get_uuid()]:
                continue
            list_box_volume = ListBoxRowVolume(parent_drive_row=self, volume=v)
            self.childs.append(list_box_volume)
            self.parent.insert(list_box_volume, self.separator.get_parent().get_index() + 2)

    def remove_child(self, child:ListBoxRowVolume):
        self.parent.remove(child)
        self.childs.remove(child)

    def clear_childs(self):
        for c in self.childs:
            self.remove_child(c)


    def set_drive(self, drive:Gio.Drive):
        self.gdrive = drive
        self.set_label(drive.get_name())

class ListBoxRowsDrives():

    parent:Gtk.ListBox = None
    drives:dict[ListBoxRowDrive] = {}
    insert_after_pos = None

    def __init__(self, insert_after_pos) -> None:

        self.insert_after_pos = insert_after_pos
        self.parent = self.insert_after_pos.get_parent().get_parent()

    def display(self):
        monitor = Gio.VolumeMonitor.get()
        drives = Gio.VolumeMonitor.get_connected_drives(monitor)
        for d in reversed(drives):
            self.add_drive(d)


    def add_drive(self, gdrive:Gio.Drive):
        row = ListBoxRowDrive(separator=self.insert_after_pos, gdrive=gdrive )
        self.parent.insert(row, self.insert_after_pos.get_parent().get_index() + 1)
        row.list_volumes()
        #self.drives.append(row)
        self.drives[gdrive.get_identifier(
            Gio.DRIVE_IDENTIFIER_KIND_UNIX_DEVICE
        )] = row

    def remove_drive(self, gdrive:Gio.Drive):
        row:ListBoxRowDrive = self.drives[gdrive.get_identifier(
            Gio.DRIVE_IDENTIFIER_KIND_UNIX_DEVICE
        )]
        if not row:
            print("Drive " + gdrive.get_name()  + " not listed.")
            return
        row.remove_child

def add_dir(dir_path, list_box, icon_name:str=None, _class=ListBoxRowDir, _name:str=None, after:Gtk.ListBoxRow=None):

    if not icon_name:
        icon_name = "folder-symbolic"

    if not dir_path.endswith("/"):
        dir_path += "/"

    g_file = Gio.File.new_for_path(dir_path)
    row = _class(gfile=g_file, icon_name=icon_name)

    if after:
        list_box.insert(row, after.get_index() + 1)
    else:
        list_box.append(row)

    return row

def add_default_dirs(win, list_box:ListBoxDirs):


    add_dir(
        dir_path=GLib.get_home_dir(),
        list_box=list_box,
        icon_name="user-home-symbolic"
    )

    icon_map = {
        GLib.UserDirectory.DIRECTORY_DESKTOP      : "user-desktop-symbolic",
        GLib.UserDirectory.DIRECTORY_DOCUMENTS    : "folder-documents-symbolic",
        GLib.UserDirectory.DIRECTORY_DOWNLOAD     : "folder-download-symbolic",
        GLib.UserDirectory.DIRECTORY_MUSIC        : "folder-music-symbolic",
        GLib.UserDirectory.DIRECTORY_VIDEOS       : "folder-videos-symbolic",
        GLib.UserDirectory.DIRECTORY_PICTURES     : "folder-pictures-symbolic",
        GLib.UserDirectory.DIRECTORY_TEMPLATES    : "folder-templates-symbolic",
        GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE : "folder-publicshare-symbolic",
    }


    for d in enumerate(GLib.UserDirectory.__enum_values__):
        dir_id = d[0]
        dir_path = GLib.get_user_special_dir(dir_id)

        if not dir_path:
            continue

        # dir_path = (dir_path.replace("file://", "")).strip().split()[0]        
        if not os.path.exists(dir_path):
            continue
        icon_name = icon_map[dir_id] if dir_id in icon_map else None
        add_dir(
            dir_path=dir_path,
            list_box=list_box,
            icon_name=icon_name
        )

    list_box.add_separator()

    add_dir(
        dir_path=GLib.get_tmp_dir(),
        list_box=list_box
    )
    add_dir(
        dir_path="/",
        list_box=list_box
    )
def add_dir(dir_path, list_box, icon_name:str=None, _class=ListBoxRowDir, _name:str=None, after:Gtk.ListBoxRow=None):

    if not icon_name:
        icon_name = "folder-symbolic"

    if not dir_path.endswith("/"):
        dir_path += "/"

    g_file = Gio.File.new_for_path(dir_path)
    row = _class(gfile=g_file, icon_name=icon_name)

    if after:
        list_box.insert(row, after.get_index() + 1)
    else:
        list_box.append(row)

    return row

def add_default_dirs(win, container_list:ListBoxDirs):


    add_dir(
        dir_path=GLib.get_home_dir(),
        list_box=container_list,
        icon_name="user-home-symbolic"
    )

    icon_map = {
        GLib.UserDirectory.DIRECTORY_DESKTOP      : "user-desktop-symbolic",
        GLib.UserDirectory.DIRECTORY_DOCUMENTS    : "folder-documents-symbolic",
        GLib.UserDirectory.DIRECTORY_DOWNLOAD     : "folder-download-symbolic",
        GLib.UserDirectory.DIRECTORY_MUSIC        : "folder-music-symbolic",
        GLib.UserDirectory.DIRECTORY_VIDEOS       : "folder-videos-symbolic",
        GLib.UserDirectory.DIRECTORY_PICTURES     : "folder-pictures-symbolic",
        GLib.UserDirectory.DIRECTORY_TEMPLATES    : "folder-templates-symbolic",
        GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE : "folder-publicshare-symbolic",
    }


    for d in enumerate(GLib.UserDirectory.__enum_values__):
        dir_id = d[0]
        dir_path = GLib.get_user_special_dir(dir_id)

        if not dir_path:
            continue

        # dir_path = (dir_path.replace("file://", "")).strip().split()[0]        
        if not os.path.exists(dir_path):
            continue
        icon_name = icon_map[dir_id] if dir_id in icon_map else None
        add_dir(
            dir_path=dir_path,
            list_box=container_list,
            icon_name=icon_name
        )

    container_list.add_separator()

    add_dir(
        dir_path=GLib.get_tmp_dir(),
        list_box=container_list
    )
    add_dir(
        dir_path="/",
        list_box=container_list
    )

_bookmarks_sep = None
def add_bookmarked_dirs(win, list_box:ListBoxDirs):
    global _bookmarks_sep
    if _bookmarks_sep is None:
        _bookmarks_sep = list_box.add_separator()
    bookmardked_dirs = Bookmarks.get_bookarks()[::-1]
    for dir_path in bookmardked_dirs:
        add_dir(dir_path, list_box, _class=ListBoxRowBookmark, after=_bookmarks_sep.get_parent())

def clear_bookmarked_dirs(win):
    items = []
    for row in win.panel.list_box:
        if isinstance(row, ListBoxRowBookmark):
            items.append(row)
    for row in items:
        row.get_parent().remove(row)

# from gi.repository import Gio
# gi.require_version('Gio', '2.0')

def add_history_dirs(win, list_box):
    recent_manager = Gtk.RecentManager()
    recent_items = recent_manager.get_items()

    directories = []

    for item in recent_items:
        uri = item.get_uri()
        if uri.startswith('file://'):
            # Convert to a local path and check if it's a directory
            local_path = Gio.File.new_for_uri(uri).get_path()
            if os.path.isdir(local_path):
                add_dir(dir_path=local_path, list_box=list_box)


def init_panel(win):

    panel_box = Gtk.Box()
    win.panel = panel_box

    from Mounter import init_mounter
    init_mounter(win)

    list_box = ListBoxDirs()

    def reset_bookmarks():
        clear_bookmarked_dirs(win)
        add_bookmarked_dirs(win, list_box)

    Bookmarks.sub_bookmark_changes(lambda *args: GLib.idle_add(reset_bookmarks))

    win.panel.list_box = list_box

    panel_box.win = win
    panel_box.set_orientation(Gtk.Orientation.HORIZONTAL)
    panel_box.set_hexpand(False)

    # panel_box.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC) 
    
    win.paned.set_start_child(
        panel_box,
    )

    def row_selected(list_box, row, user_data=None):
        win.change_dir(row.path if isinstance(row, ListBoxRowDir) else None)

    panel_box.append(list_box)
    list_box.connect("row-selected", row_selected)

    add_default_dirs(win, list_box)
    add_bookmarked_dirs(win, list_box)
    #add_drives(win, list_box)
    
    separator_drives = list_box.add_separator()
    list_drives = ListBoxRowsDrives(insert_after_pos=separator_drives)
    list_drives.display()

    def set_dir_as_active(*args):
        curr_path = win.get_current_tab().media_dir.path
        for child in list_box:
            if child.get_selectable() and isinstance(child, ListBoxRowDir) and child.get_path() == curr_path:
                break

    list_box.add_separator()
    # add_history_dirs(win, list_box)

    win.connect(
            "page-changed", 
            lambda *args: set_dir_as_active(args)
    )


