
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gdk, GLib, GObject 

def _add_filter(self):
    filter_entry = Gtk.Entry()
    filter_entry.add_css_class("filter")
    filter_entry.set_placeholder_text("Haku")
    filter_entry.set_hexpand(True)

    filter_entry.set_icon_from_icon_name(
        Gtk.EntryIconPosition.PRIMARY,
        "system-search-symbolic"
    )
    self.overlay.content.append(filter_entry)
    self.filter_entry = filter_entry


    def filter_activated(*args):
        tab = self.tab
        tab.q = filter_entry.get_buffer().get_text()

        self.window.clear_tab(tab)
        self.window.display_page(0)
        
    filter_entry.connect("activate", filter_activated)

    trigger = Gtk.ShortcutTrigger.parse_string("<Control>f")
    def action_cb(*args):
        filter_entry.grab_focus()
    action = Gtk.CallbackAction.new(lambda *args: GLib.idle_add(action_cb))
    shortcut = Gtk.Shortcut.new(trigger, action)
    self.add_shortcut(shortcut)
