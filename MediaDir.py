from PIL import Image, ImageOps

from os import path, stat, listdir, mkdir, stat
from stat import S_ISDIR, S_ISREG

import subprocess

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


from math import ceil
import re
import ThreadPool
from dataclasses import dataclass
from typing import List
from pathlib import Path

from enum import Enum
from appdirs import user_cache_dir

from gi.repository import Gio


class Cv2Wrapper:
    VideoCapture:callable = None
    resize:callable = None
    imwrite:callable = None
    INTER_AREA = None

_cv2 = None

def get_cv2(cb:callable=None):
    global _cv2
    if _cv2:
        return _cv2

    def _load():
        from cv2 import VideoCapture, resize, imwrite, INTER_AREA
        res = Cv2Wrapper()
        res.VideoCapture = VideoCapture
        res.resize = resize
        res.imwrite = imwrite
        res.INTER_ARE = INTER_AREA

        return res

    _cv2 = _load()

    return _cv2


MediaType = Enum('TYPE', [
    "IMAGE",
    "VIDEO",
    "AUDIO",
    "DOC",
    "DIR",
    "EXECUTABLE",
    "UNKNOWN",
])

THUMBED_TYPES = ([
    MediaType.IMAGE,
    MediaType.VIDEO,
    MediaType.AUDIO
])

# If set True thumbnail will be stored in child directory called thumbs. 
# Otherwise centered THUMB_DIR will be used.
STORE_THUMBS_IN_CHILD_DIR=False

# user_cache_dir() could be ~/.cache/
THUMB_DIR =  user_cache_dir() + "/thumbsx/"
THUMB_DIMENSIONS = 128
FILES_PER_PAGE = 1024

print("Storing thumbs in " + THUMB_DIR)

# Change to set if list grows bigger and hashtable turns to be more benefitial.
SUPPORTED_IMG_EXTS      = ([".jpg", ".jpeg", ".jpe", ".png", ".gif", ".webp", ".bmp", ".jfif", ".avif", ".svg"])
SUPPORTED_AUDIO_EXTS    = ([".mp3", ".ogg", ".wav", ".flac", ".opus", ".m4a", ".avi"])
SUPPORTED_DOC_EXTS      = ([".txt", ".pdf", ".doc", ".odf"])
SUPPORTED_VID_EXTS      = ([".webm", ".mp4", ".mov", ".mkv", ".m4v"])
SUPPORTED_EXEC_EXTS     = ([".sh", ".py", ".bin", ".exe"])


ALL_SUPPORTED_EXTS = set(SUPPORTED_IMG_EXTS + SUPPORTED_AUDIO_EXTS + SUPPORTED_DOC_EXTS + SUPPORTED_VID_EXTS + SUPPORTED_EXEC_EXTS)

_ext_to_mimes_map = {}

def map_exts_to_mimes(mime_type_file="/etc/mime.types"):
    mimes_map = {}
    f = open(mime_type_file)
    while True:
        line = f.readline()
        if not line:
            break
        splitd = line.split()
        if len(splitd) < 2:
            continue
        mime = splitd[0]
        for ext in (splitd[1:]):
            mimes_map[ext] = mime
    return mimes_map

def guess_mime_by_ext(ext:str, mimes_map=None):
    if not mimes_map:
        global _ext_to_mimes_map
        mimes_map = _ext_to_mimes_map
    if not ext:
        return None
    if ext[0] == ".":
        ext = ext[1:]
    return mimes_map[ext] if ext in mimes_map else None

_ext_to_mimes_map = map_exts_to_mimes()


class BaseFileClass():

    _ext:str    = None
    _type:str   = None

    def get_path(self) -> str:
        pass

    def get_name(self) -> str:
        pass

    def get_name_extless(self) -> str:

        name = self.get_name()
        ext_start = name.rfind(".")
        if ext_start == -1:
            return name
        extless = name[:ext_start]

        return extless

    def get_ext(self) -> str:
        if self._ext:
            return self._ext
        
        ext_start = self.get_name().rfind(".")
        if ext_start == -1 or ext_start == 0:
            return None
        
        self._ext = self.name[ext_start:].lower()
        return self._ext

    def resolve_type(self):
        ext = self.get_ext()

        if ext:
            if ext in SUPPORTED_IMG_EXTS:
                self._type = MediaType.IMAGE
            elif ext in SUPPORTED_AUDIO_EXTS:
                self._type = MediaType.AUDIO
            elif ext in SUPPORTED_DOC_EXTS:
                self._type = MediaType.DOC
            elif ext in SUPPORTED_VID_EXTS:
                self._type = MediaType.VIDEO
            elif ext in SUPPORTED_EXEC_EXTS:
                self._type = MediaType.EXECUTABLE
            if ext == ".d":
                self._type = MediaType.DIR
            #else:
            #    self.type = MediaType.UNKNOWN
        
        if not self._type:
            if self.assume_is_dir() and path.isdir(self.get_path()):
                self._type = MediaType.DIR

    def get_type(self) -> MediaType:
        if not self._type:
            self.resolve_type()
        return self._type

    def assume_is_dir(self):
        name = self.get_name()
        ext_start = name.rfind(".")
        if ext_start == -1 or name.endswith(".d"):
            return True
        
        ext = self.get_ext()

        if not ext or not ext.lower() in ALL_SUPPORTED_EXTS:
            return path.isdir(self.get_path())

        return False

@dataclass
class MediaFile(BaseFileClass):

    parent: any
    name:   str
    stat:   any
    type:   MediaType = MediaType.UNKNOWN

    def get_stat(self):
        if not self.stat:
            try:
                self.stat = stat(self.get_path())
            except  OSError or FileNotFoundError:
                return None
        return self.stat

    def get_name(self):
        return self.name

    def get_ext(self):
        ext_start = self.name.rfind(".")
        if ext_start == -1 or ext_start == 0:
            return None
        ext = self.name[ext_start:].lower()
        return ext

    def get_path(self):
        return self.parent.path + self.name

    def get_desired_thumb_path(self):
        stat = self.get_stat()
        filename = str(stat.st_ino) + "_" + str(stat.st_dev) + "." + str(stat.st_mtime)
        thumb_folder = THUMB_DIR 

        if STORE_THUMBS_IN_CHILD_DIR:
            thumb_folder = self.parent.path + "/thumbs/"

        if not path.exists(thumb_folder):
            mkdir(thumb_folder)

        desired_file = thumb_folder + filename + ".webp"
        return desired_file 


    def get_thumb_file(self):
        if self.get_type() not in [MediaType.IMAGE, MediaType.VIDEO, MediaType.AUDIO]:
            return None

        desired_path = self.get_desired_thumb_path()

        if (path.isfile(desired_path)):
            if (path.getsize(desired_path)):
                return desired_path
            return None

        match self.get_type():
            case MediaType.IMAGE:
                try:
                    with ImageOps.exif_transpose(Image.open(self.get_path())) as image:
                        #image = ImageOps.exif_transpose(image_tmp)
                        image.thumbnail((THUMB_DIMENSIONS, THUMB_DIMENSIONS))
                        image.save(desired_path, "WEBP")
                except Exception as err:
                    print(err)
                    return None

            case MediaType.VIDEO:
                try:
                    print("thumbing vid")
                    """
                    input = self.get_path()
                    cmd = f"ffmpeg  -i '{input}' -y -vframes 1 -vf scale='{THUMB_DIMENSIONS}:-2' {desired_path}" 
                    subprocess.run(
                        #cmd, 
                        ["ffmpeg", "-i", input, "-y", "-frames", "1", "-vf", f"scale='{THUMB_DIMENSIONS}:-2'", desired_path], 
                        stdout=subprocess.DEVNULL,
                        #stderr=subprocess.STDOUT,
                        shell=False
                    )
                    """
                    cv2 = get_cv2(None)
                    input = self.get_path()
                    try:
                        vidcap = cv2.VideoCapture(input)
                        succ, image = vidcap.read()
                        if succ:
                            scale_rate = min(1, THUMB_DIMENSIONS / max(image.shape[0], image.shape[1]))
                            dims = (int(image.shape[1] * scale_rate), int(image.shape[0] * scale_rate))
                            scaled_img = cv2.resize(image, dims)
                            cv2.imwrite(desired_path, scaled_img)
                    except Exception as e:
                        print("OpenCV error")
                        print(e)
                    finally:
                        vidcap.release()
                except Exception as err:
                    print("??")
                    print(err)
                    return None
            case MediaType.AUDIO:
                try:
                    input = self.get_path()
                    cmd = f"ffmpeg -i '{input}' -y -pix_fmt rgb8  -vf scale='{THUMB_DIMENSIONS}:-2' -an {desired_path}"
                    subprocess.run(
                        cmd, 
                        stdout=subprocess.DEVNULL,
                        stderr=subprocess.DEVNULL,
                        shell=True
                    )
                except Exception as err:
                    print(err)
                    print(cmd)
                    return None

        if not path.isfile(desired_path):
            # Make emty file so unnecessary future attempts to generate thumbs will be avoided. 
            Path(desired_path).touch()
            return None
        # Little time for another thread, thought subprocess.run() and PIL should release the GIL. 
        time.sleep(0.05) 

        return desired_path


MediaSorting = Enum("MediaSorting", [
    "NAME",
    "TYPE",
    "EXT",
    "MTIME",
    "ATIME",
    "CTIME",
    "SIZE",
])

import time

DEFAULT_SORTING = MediaSorting.CTIME

@dataclass
class MediaDir():

    path            : str
    count           : int
    pages           : int
    files_all       : list[MediaFile]
    files_listed    : list[MediaFile] 
    files_per_page  : int = FILES_PER_PAGE 
    sorting         : MediaSorting = DEFAULT_SORTING 
    sort_reversed   : bool = False
    filter_str      : str = None
    filter_re       : any = None
    display_hiddens : bool = False
    l_hiddens       : bool = False
    dirty           : bool = False

    observer        : any = None


    #TODO: For fun check if it's reasonable to load and sort directory with
    # C or C++ module instead in case of huge directory.
    # Or better avoid sorting and stat() if there are like +100k files unless asked.
    # Because of GIL it seems like the multithreading done here is even harmul.
    # Launch the static method in single thread while providing the loaded files in batches
    # to callback if no sorting used.
    @staticmethod
    def load_from_path(
            dir_path:str, 
            threaded=False, 
            worker:ThreadPool=None,
            sorting:MediaSorting=DEFAULT_SORTING,
        ):

        st = time.time()
        media_dir = MediaDir(
                path            = dir_path,
                files_all       = [], 
                files_listed    = [],
                count           = 0, 
                pages           = 0, 
                sorting         = sorting,
        )
        
        def file_mapper(f):
            stat_res = None
            media_file = MediaFile(media_dir, 
                                   name=f, 
                                   type=None, 
                                   stat=stat_res)
            return media_file

        close_worker = False
        if threaded and not worker:
            print("Starting worker")
            worker = ThreadPool.ThreadPool()
            worker.start()
            close_worker = True

        def load_batch(files:List[str], m_dir):
            t = list(map(file_mapper, files)) 
            m_dir.files_all.extend(t)

        files = listdir(dir_path)

        while len(files):
            batch = files[:512]
            if threaded:
                worker.add_job(
                    target=load_batch,
                    args=[batch, media_dir]
                )
            else:
                load_batch(batch, media_dir)
            files = files[513:]
       
        if worker:
            worker.wait_tasks_to_finish()
            if close_worker:
                worker.stop()
        
        media_dir.files_listed = media_dir.files_all

        media_dir.count = len(media_dir.files_all)
        media_dir.pages = ceil(media_dir.count / media_dir.files_per_page)

        sort_time_start = time.time()
        media_dir.sort()
        sort_time_end = time.time()

        et = time.time()
        elapsed_time = et - st

        sort_time = sort_time_end - sort_time_start


        print('Loaded ' + str(media_dir.count) + ' files from \''+ dir_path + '\' in time ', elapsed_time, 'seconds, (sorting ', sort_time, ')')

        return media_dir

    def sort(self,
             sorting:MediaSorting=DEFAULT_SORTING,
             worker=None,
             cb:callable=True,
             cb_params:any=None
    ):

        if worker:
            worker.add_job(
                target=self.sort,
                args=[
                    sorting
                ],
                cb=cb,
                cb_params=cb_params
            )
            return

        sort_orig = self.sorting
        if sorting:
            self.sorting = sorting

        match self.sorting:
            case MediaSorting.NAME:
                self.files_listed.sort(key=lambda f: f.get_name_extless())
                #self.files.listed_sort(key=len, reversed=True)
            #case MediaSorting.TYPE:
            #    self.files_listed.sort(key=lambda f: f.get_type() or -1)
            case MediaSorting.EXT:
                self.files_listed.sort(key=lambda f: f.get_ext() or '')
            case MediaSorting.MTIME:
                self.files_listed.sort(key=lambda f: f.get_stat().st_mtime if f.get_stat() else 0)
            case MediaSorting.ATIME:
                self.files_listed.sort(key=lambda f: f.get_stat().st_atime if f.get_stat() else 0)
            case MediaSorting.CTIME:
                self.files_listed.sort(key=lambda f: f.get_stat().st_ctime if f.get_stat() else 0)
                self.files_listed.reverse()
            case MediaSorting.SIZE:
                self.files_listed.sort(key=lambda f: f.get_stat().st_size if f.get_stat() else 0)
            case _:
                self.sorting = sort_orig
                raise Exception("Unknown sort type")

        if self.sort_reversed:
            self.files_listed.reverse()

        self.files_listed.sort(key=lambda f: f.assume_is_dir() == False)

        return self

    def get_items(self, page:int=0, q:str=None, re_q=None): 

        start = page*self.files_per_page 
        result = self.files_listed
       
        filter_changed = False

        if self.filter_str != q:
            self.filter_str = q
            self.files_listed = self.files_all
            filter_changed = True

        if self.filter_re != re_q:
            self.filter_re = re_q
            filter_changed = True

        if filter_changed:
            result = self.files_all

        if filter_changed and q and len(q):
            result = list(filter(lambda f: q in f.name.lower(), result))
       
        def re_filter_cb(file:MediaFile):
            extless = file.get_name_extless()
            return (
                   re.fullmatch(re_q, file.name)
                or re.fullmatch(re_q, extless)
            )
        
        if filter_changed and re_q:
            result = list(filter(re_filter_cb, result))
        
        def hides_filter_cb(file:MediaFile):
            return file.name[0] != "."

        if self.display_hiddens is not True:
            result = list(filter(hides_filter_cb, result))



        self.files_listed = result
        self.count = len(result)
        self.pages = ceil(self.count / self.files_per_page)

        return list(result[start:start+self.files_per_page]) 

    def get_parent_dir(self):
        path = Path(self.path)
        return path.parent.absolute()

    def uninstall_monitor(self):
        if self.observer:
            self.observer.unschedule_all()
            self.observer.stop()
            self.observer = None

    def install_monitor(self,
                        callback_file_created=None,
                        callback_file_removed=None,
                        callback_file_edited=None):
        media_dir:MediaDir = self

        class MyHandler(FileSystemEventHandler):
            def on_created(self, event):
                media_file = MediaFile(
                    parent=media_dir,
                    name=Path(event.src_path).name,
                    stat=None
                )
                media_dir.files_all.insert(0, media_file)

                if callback_file_created:
                    callback_file_created(media_file)

                return super().on_created(event)

            def on_deleted(self, event):
                f_name = Path(event.src_path).name

                for media_file in media_dir.files_all:
                    if media_file.name == f_name:
                        media_dir.files_all.pop(media_dir.files_all.index(media_file))
                        if callback_file_removed:
                            callback_file_removed(media_file)

                return super().on_deleted(event)
        

        print("Asennetaan vahtikoira kansioon " + self.path)
        event_handler = MyHandler()

        self.observer = Observer()
        self.observer.schedule(event_handler, path=self.path, recursive=False)
        self.observer.start()

        print("...asennettu")
