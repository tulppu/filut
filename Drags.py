from gi.repository import Gtk, Gdk, Gio, GObject, GLib

import GtkDialogs
import GtkFileList
import GtkFileListItem
import MediaDir


def files_on_drop(drop_target:Gtk.DropTarget, filelist:Gdk.FileList, x, y):

    self:GtkFileList.FileList = drop_target.parent

    def file_copied_cb(*args):
        file = args[0]
        f_name = file.get_basename()

        media_file = MediaDir.MediaFile(parent=self.tab.media_dir, name=f_name, type=None, stat=None)        
        new_column = self.tab.win.build_file_column(media_file)
        new_column.request_thumb(win=self.tab.win)

        self.append(new_column)
        
    def on_file_copied(source, task, dest=None):
        try:
            dest_path = self.get_media_dir().path + source.get_basename()
            file_item = self.get_child_by_path(dest_path)
            if file_item:
                self.unselect_all()
                self.select_child(file_item)
                file_item.grab_focus()
                file_item.request_thumb(self.get_media_dir(), file_item.media_file, self.window)
        except Exception as err:
            GtkDialogs.show_error(str(err), self.window)
            print(err)

    for f in filelist.get_files():
        
        dest = Gio.File.new_for_path(
                    self.tab.media_dir.path + f.get_basename()
        )

        f.copy_async(
                dest,
                Gio.FileCopyFlags.NONE,
                GLib.PRIORITY_DEFAULT,
                None,
                None,
                on_file_copied,
        )


# https://docs.gtk.org/gtk4/class.DragSource.html
def drag_prepare(drag_source, x:int, y:int, widget):

    flowchild = widget.get_child_at_pos(x,y)
    drag_source.flowchild = flowchild 

    if not flowchild or not isinstance(flowchild, GtkFileListItem.FilesListItem):
        return

    path = flowchild.get_parent().tab.media_dir.path + flowchild.file_name
    gfile = Gio.File.parse_name(path)

    path_escaped = GObject.Value(GObject.TYPE_STRING)
    path_escaped.set_string("'" + path + "'")

    gfile_value = GObject.Value(Gio.File)
    gfile_value.set_object(gfile)
    #gfile_value.set_gtype(GObject.TYPE_OBJECT)

    #files = Gdk.FileList.new_from_list([gfile])

    return Gdk.ContentProvider.new_union([
        Gdk.ContentProvider.new_for_value(gfile_value),
        Gdk.ContentProvider.new_for_value(path_escaped),
    ])


def drag_begin(drag_source, _widged, _öö):
    paint_source = drag_source.flowchild
    if paint_source.thumb:
        paint_source = paint_source.thumb
    paintable = Gtk.WidgetPaintable.new(paint_source)
    Gtk.DragSource.set_icon(drag_source, paintable, 0, 0)
