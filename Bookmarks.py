import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import GLib, Gio

import os

bookmarks_file = GLib.get_user_config_dir() + "/gtk-3.0/bookmarks"
if not os.path.isfile(bookmarks_file):
    from pathlib import Path
    Path(bookmarks_file).touch()

_changed_subscribers = []
_monitor = Gio.File.new_for_path(bookmarks_file).monitor_file(Gio.FileMonitorFlags.NONE)

def sub_bookmark_changes(cb:callable):
    global _changed_subscribers
    _changed_subscribers.append(cb)

def _changed(*args):
    global _changed_subscribers
    for cb in _changed_subscribers:
        cb()
_monitor.connect("changed", _changed)

def get_bookarks():
    bookmarks = []
    with open(bookmarks_file, "r") as file:
        for uri in file:
            dir_path = None
            if uri.startswith("file://"):

                filename = GLib.filename_from_uri(uri)
                dir_path = filename[0]
                dir_path = (dir_path.replace("file://", "")).strip().split()[0]        
        
                bookmarks.append(dir_path)

    return bookmarks

def is_bookmarked_dir(dir_path:str) -> bool:
    gfile = Gio.File.new_for_path(dir_path)
    if gfile.get_path() in get_bookarks():
        return True
    return False

def add_bookmark_dir(dir_path:str):
   
    gfile = Gio.File.new_for_path(dir_path)
    if not gfile.get_path() or is_bookmarked_dir(dir_path):
        return

    with open(bookmarks_file, "a") as f:
        f.write(gfile.get_uri() + "\n")

def remove_bookmark_dir(removed_dir:str, win=None):
    gfile = Gio.File.new_for_path(removed_dir)
    if not gfile.get_path() or not is_bookmarked_dir(removed_dir):
        print("'{}' can't be unbookmarked!".format(removed_dir))
        return
    bookmarks = get_bookarks()
    file_content = ""
    with open(bookmarks_file, "w") as file:
        for mark in bookmarks:
            if not os.path.samefile(mark, removed_dir):
                gfile = Gio.File.new_for_path(mark)
                file_content += gfile.get_uri() + "\n"
            else:
                print("Poistetaan {}".format(mark))
        file.write(file_content)
