from typing import List
from os.path import isdir
from os import fork, popen, spawnl, P_NOWAIT

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, GLib, Gio

context_menu = None

from enum import Enum


class MenuAction():
    def __init__(self, action_name, action:Gio.SimpleAction, handler:callable):
        self.action_name = action_name
        self.action = action
        self.handler = handler

class MenuAction():
    COPY        = "app.copy"
    COPY_PATH   = "app.copy_path"
    COPY_NAME   = "app.copy_name"
    OPEN_WITH   = "app.open_with"
    CONVERT     = "app.convert"
    RENAME      = "app.rename"
    TRASH       = "app.trash"

def get_context_menu():
    global context_menu
    return context_menu

# https://gitlab.gnome.org/GNOME/gssdp/-/blob/master/tools/main-window.c#L731
def pop_context_menu(filesbox, item, x, y):

    #from import FilesBoxChild, FilesBox
    from GtkFileListItem import FilesListItem
    from GtkFileList import FileList

    item:FilesListItem= item
    filesbox:FileList = filesbox

    global context_menu
    #context_menu = None

    if context_menu:
        context_menu.get_parent().remove(context_menu)
        context_menu.run_dispose()
        context_menu = None

    context_menu = Gtk.PopoverMenu()
    context_menu.menu = Gio.Menu()
    context_menu.add_css_class("context-menu")

    if isinstance(item, FilesListItem):
        g_info = item.get_g_info()
        mime = g_info.get_content_type()

        default_app = Gio.AppInfo.get_default_for_type(mime, False)

        open_with_menu = Gio.Menu()

        if mime == "inode/directory":
            context_menu.menu.append("_Avaa")

        if default_app:
            #menu.append("Open with " + default_app.get_name(), None)
            open_with_default = Gio.MenuItem.new("Open with " + default_app.get_name()) 
            open_with_default.set_action_and_target_value("app." + MenuAction.OPEN_WITH, GLib.Variant.new_string(default_app.get_id()))

            context_menu.menu.append_item(open_with_default)

        rest_apps = Gio.AppInfo.get_recommended_for_type(mime)
        if len(rest_apps):
            for app in rest_apps:
                m_item = Gio.MenuItem.new(app.get_name()) 
                m_item.set_action_and_target_value("app." + MenuAction.OPEN_WITH, GLib.Variant.new_string(app.get_id()))
                m_item.set_icon(app.get_icon())
                open_with_menu.append_item(m_item)

            context_menu.menu.append_submenu("_Open with", open_with_menu)

        audio_ext_list = [".mp3", ".ogg", ".opus", ".m4a", ".wma", ".wav"]
        if  item.media_file.get_ext() in audio_ext_list or mime.startswith("audio/"):
            convert_to_menu = Gio.Menu()
            for ext in audio_ext_list:
                if ext == item.media_file.get_ext():
                    continue 
                convert_to_item = Gio.MenuItem.new("..to " + ext)
                convert_to_item.set_action_and_target_value("app." + MenuAction.CONVERT, GLib.Variant.new_string(ext))
                convert_to_menu.append_item(convert_to_item)

            context_menu.menu.append_submenu("_Convert to", convert_to_menu)

        selection_copy = Gio.Menu()

        selection_copy.append("_Copy", "app." + MenuAction.COPY)
        selection_copy.append("Copy _path", "app." + MenuAction.COPY_PATH)
        selection_copy.append("Copy _name", "app." + MenuAction.COPY_NAME)
        selection_copy.append("_Rename", "app." + MenuAction.RENAME)
        selection_copy.append("_Trash", "app." + MenuAction.TRASH)

        context_menu.menu.append_section(None, selection_copy)

        item.get_parent().unselect_all()
        item.get_parent().select_child(item)

    ctx_parent = context_menu.get_parent()
    if ctx_parent:
        ctx_parent.remove(context_menu)

    item.container.append(context_menu)

    context_menu.set_menu_model(context_menu.menu)
    context_menu.popup()
    context_menu.popped_item = item

    return context_menu.menu



def init_context_menu_actions(win):
    import ContextMenuActions

    app = win.get_application()

    ContextMenuActions.init(app, get_context_menu)

    def _register_action(action:Gio.SimpleAction, executor_cb:callable) -> None:
        action.connect("activate", executor_cb)
        Gio.ActionMap.add_action(win.get_application(), action)

    action_open_with    = Gio.SimpleAction.new(MenuAction.OPEN_WITH, GLib.VariantType.new("s"))
    action_convert      = Gio.SimpleAction.new(MenuAction.CONVERT, GLib.VariantType.new("s"))
    action_copy         = Gio.SimpleAction.new(MenuAction.COPY)
    action_copy_path    = Gio.SimpleAction.new(MenuAction.COPY_PATH)
    action_copy_name    = Gio.SimpleAction.new(MenuAction.COPY_NAME)
    action_rename       = Gio.SimpleAction.new(MenuAction.RENAME)
    action_trash        = Gio.SimpleAction.new(MenuAction.TRASH)

    _register_action(action_open_with,   ContextMenuActions.execute_open_with)
    _register_action(action_convert,     ContextMenuActions.execute_convert)
    _register_action(action_copy,        ContextMenuActions.execute_open_with)
    _register_action(action_copy_path,   ContextMenuActions.execute_copy_path)
    _register_action(action_copy_name,   ContextMenuActions.execute_copy_name)
    _register_action(action_rename,      ContextMenuActions.execute_rename)
    _register_action(action_trash,       ContextMenuActions.execute_trash)
