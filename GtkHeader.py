
import gi

import GtkDialogs
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, GLib, Gio

from MediaDir import MediaSorting
import Bookmarks

def set_path(win):
    dir = win.get_media_dir()
    if not dir:
        return
    curr_path = dir.path
    win.header.path.get_buffer().set_text(curr_path, len(curr_path))

    dir_name = "/" if curr_path is "/" else curr_path.split("/")[-2]
    win.set_title(dir_name)



def update_header(win, *args):
    current_dir = win.get_media_dir()

    if not current_dir:
        return

    header = win.header

    header.btn_go_forward.set_sensitive(False)
    header.btn_go_back.set_sensitive(False)
    header.btn_go_up.set_sensitive(False)

    if current_dir.get_parent_dir():
        header.btn_go_up.set_sensitive(True)

    update_bookmark_icon(win)
    set_path(win)

def update_bookmark_icon(win):
    current_dir = win.get_media_dir()
    header = win.header
    if not Bookmarks.is_bookmarked_dir(current_dir.path):
        header.btn_bookmark.set_icon_name("non-starred-symbolic")
        header.btn_bookmark.remove_css_class("enabled")
    else:
        header.btn_bookmark.set_icon_name("starred-symbolic")
        header.btn_bookmark.add_css_class("enabled")

def create_new_folder(win):
    def on_dialog_response(response:Gtk.ResponseType, new_folder_name:str, dialog_win):
        if response != Gtk.ResponseType.OK:
            # dialog.destroy()
            return

        win.get_current_tab()

        # new_folder_name = dialog.entry.get_text()

        if not len(new_folder_name):
            return

        curr_dir_path = win.get_media_dir().path
        import os
        new_folder_path = os.path.join(curr_dir_path, new_folder_name)

        gfile = Gio.File.new_for_path(new_folder_path)
        try:
            gfile.make_directory()
        except GLib.Error as err:
            GtkDialogs.show_error(err.message, win)

    dialog_name_picker = GtkDialogs.InputDialog(win, "Anna kansion nimi", "Kansionimi", on_dialog_response)
    dialog_name_picker.present()
    # dialog_name_picker.connect("response", on_dialog_response, win)


def select_dir_to_open(win):
    def dir_selected(dialog:Gtk.NativeDialog, res):
        if res == Gtk.ResponseType.ACCEPT:
            dir = dialog.get_file() 
            win.change_dir(dir.get_path())

    win.dir_open_dialog = Gtk.FileChooserNative.new(
        title=None,
        parent=win,
        action=Gtk.FileChooserAction.SELECT_FOLDER,

    )
    win.dir_open_dialog.connect("response", dir_selected) 
    Gtk.NativeDialog.show(win.dir_open_dialog)



def toggle_bookmark_status(win, *args):
    current_dir = win.get_media_dir()
    if not current_dir:
        return

    if not Bookmarks.is_bookmarked_dir(current_dir.path):
        Bookmarks.add_bookmark_dir(current_dir.path)
    else:
        Bookmarks.remove_bookmark_dir(current_dir.path)
    
    update_bookmark_icon(win)

def init_header(win):

    header = Adw.HeaderBar()
    header.add_css_class("header")
    header.win = win
    win.header = header
    header.reversed_order_toggle = Gtk.ToggleButton.new_with_label("Käänteinen järjestys")

    header.label = Gtk.Label()

    header.pack_start(header.label)

    header.path = Gtk.Entry()
    header.path.set_tooltip_text("Polku")
    header.path.set_width_chars(25)
    header.path.set_margin_start(7)
    header.pack_end(header.path)
    
    header.pack_end(Gtk.Separator())

    header.new_tab = Gtk.Button(label="")
    header.new_tab.set_icon_name("tab-new-symbolic")
    header.pack_end(header.new_tab)
    header.new_tab.connect("clicked", lambda *args: win.create_tab(parent=win.get_current_tab()))
    
    header.btn_terminal = Gtk.Button(label="")
    header.btn_terminal.set_icon_name("utilities-terminal-symbolic")
    header.pack_end(header.btn_terminal)
    def open_terminal(*args):
        from os import popen
        _dir = win.get_media_dir().path
        popen("exo-open --launch TerminalEmulator --working-directory \"" + _dir + "\"")
    header.btn_terminal.connect("clicked", open_terminal)

    header.pack_end(Gtk.Separator())

    header.btn_new_folder= Gtk.Button(label="")
    header.btn_new_folder.set_tooltip_text("Luo kansio")
    header.btn_new_folder.set_icon_name("folder-new-symbolic")
    header.pack_end(header.btn_new_folder)
    header.btn_new_folder.connect("clicked", lambda *args: create_new_folder(win))

    header.btn_open_folder = Gtk.Button(label="")
    header.btn_open_folder.set_icon_name("folder-open-symbolic")
    header.btn_open_folder.set_tooltip_text("Avaa kansio")
    header.pack_end(header.btn_open_folder)
    header.btn_open_folder.connect("clicked", lambda *args: header.select_dir_to_open(win))


    header.select_dir_to_open = select_dir_to_open
    #header.btn_open.connect("clicked", header.select_dir_to_open)

    header_box = Gtk.Box()
    header_box.set_orientation(Gtk.Orientation.VERTICAL)
    header_box.append(win.header)
    header_box.append(win.tab_bar)
    win.main_box.prepend(header_box)


    win.tab_view.connect(
            "notify::selected-page", 
            lambda *args: set_path(win)
    )

    win.tab_view.connect(
            "notify::selected-page", 
            lambda *args: update_header(win, args)
    )

    win.connect(
            "page-changed", 
            lambda *args: update_header(win, args)
    )


    update_header(win)
    header.path.connect("activate", lambda *args: win.change_dir()) 

    add_actions(win)
    add_sort_menu(header)
    add_back_next_buttons(win)

    header.btn_home =  Gtk.Button(label="")
    header.btn_home.set_icon_name("go-home-symbolic")
    header.btn_home.connect("clicked", lambda *args: win.change_dir(GLib.get_home_dir()))
    header.pack_start(header.btn_home)

    header.btn_refresh =  Gtk.Button(label="")
    header.btn_refresh.set_icon_name("view-refresh-symbolic")
    header.btn_refresh.connect("clicked", lambda *args: win.change_dir(
        win.get_current_tab().media_dir.path
        #win.refresh_current_dir()
    ))
    header.pack_start(header.btn_refresh)

    header.btn_bookmark =  Gtk.Button(label="")
    header.btn_bookmark.set_icon_name("starred-symbolic")
    header.btn_bookmark.add_css_class("btn-bookmark")
    header.btn_bookmark.connect("clicked", lambda* args: toggle_bookmark_status(win))
    #header.btn_bookmark.connect("clicked", lambda *args: win.change_dir(
    #    #win.refresh_current_dir()
    #))
    header.pack_start(header.btn_bookmark)

def add_back_next_buttons(win):
    header = win.header
    
    header.btn_go_forward = Gtk.Button(label="")
    header.btn_go_back = Gtk.Button(label="")
    header.btn_go_up = Gtk.Button(label="")


    header.btn_go_forward.set_sensitive(False)
    header.btn_go_back.set_sensitive(False)
    header.btn_go_up.set_sensitive(False)

    header.btn_go_forward.set_icon_name("go-next")
    header.btn_go_back.set_icon_name("go-previous")
    header.btn_go_up.set_icon_name("go-up")

    header.pack_start(header.btn_go_up)
    header.pack_start(header.btn_go_back)
    header.pack_start(header.btn_go_forward)

    #header.dir_info = Gtk.Label()
    #header.pack_start(header.dir_info)

    header.btn_go_forward.connect(
        "clicked", 
        lambda *args: win.display_page(win.get_current_page() + 1)
    )

    header.btn_go_back.connect(
        "clicked", 
        lambda *args: win.display_page(win.get_current_page() - 1)
    )

    header.btn_go_up.connect(
        "clicked", 
        lambda *args: win.change_dir(
            str(win.get_media_dir().get_parent_dir())
        )
    )


# TODO: Create Hotkeys.py or something and move there.
def add_actions(win):
    app = win.get_application()
    header = win.header

    action_open_dir = Gio.SimpleAction.new("open_dir", None)
    action_open_dir.connect("activate", header.select_dir_to_open)
    app.add_action(action_open_dir)

    app.set_accels_for_action(
        "app.open_dir",
        (
            "<Control>o",
        )
    )

    def open_new_tab(*args):
        current_dir = win.get_media_dir()
        current_tab = win.get_current_tab()

        new_tab = win.create_tab(
            dir_path=current_dir.path,
            #media_dir=media_dir,
            parent=current_tab,
        )

        #win.tab_view.reorder_page(new_tab, 0)

    #def open_tab_launcher(*args):



    action_new_tab = Gio.SimpleAction.new("open_tab", None)
    action_new_tab.connect("activate", open_new_tab)
    app.add_action(action_new_tab)

    app.set_accels_for_action(
        "app.open_tab",
        (
            "<Control>t",
        )
    )


# When clicked causes following:
"""
/usr/lib/python3/dist-packages/gi/overrides/Gio.py:42: Warning: invalid cast from 'GtkBox' to 'GtkScrolledWindow'
  return Gio.Application.run(self, *args, **kwargs)
(python3:36903): Gtk-CRITICAL **: 18:23:41.263: gtk_scrolled_window_get_child: assertion 'GTK_IS_SCROLLED_WINDOW (scrolled_window)' failed
(python3:36903): Gtk-CRITICAL **: 18:23:41.263: gtk_viewport_get_child: assertion 'GTK_IS_VIEWPORT (viewport)' failed
(python3:36903): Gtk-CRITICAL **: 18:23:41.263: gtk_stack_set_visible_child_name: assertion 'GTK_IS_STACK (stack)' failed
(python3:36903): Gtk-CRITICAL **: 18:23:41.776: gtk_scrolled_window_get_child: assertion 'GTK_IS_SCROLLED_WINDOW (scrolled_window)' failed
(python3:36903): Gtk-CRITICAL **: 18:23:41.776: gtk_viewport_get_child: assertion 'GTK_IS_VIEWPORT (viewport)' failed
(python3:36903): Gtk-CRITICAL **: 18:23:41.776: gtk_stack_set_visible_child_name: assertion 'GTK_IS_STACK (stack)' failed
"""
def add_sort_menu(header):
   
    win = header.win

    header.pack_end(Gtk.Separator())

    header.burger = Gtk.MenuButton()
    header.burger.set_icon_name("open-menu-symbolic")
    header.burger.set_tooltip_text("Järjestely")
    header.pack_end(header.burger)

    header.popover = Gtk.PopoverMenu()
    header.burger.set_popover(header.popover)
    header.menu = Gio.Menu.new()
    header.popover.set_menu_model(header.menu)
    #header.popover.set_menu_model(menu)

    menu_holder = Gtk.Box(spacing=0)
    menu_holder.set_margin_top(10)
    menu_holder.set_margin_start(8)
    menu_holder.set_margin_end(8)
    menu_holder.set_orientation(Gtk.Orientation.VERTICAL);

    header.popover.set_child(menu_holder)

    sort_container = Gtk.Box()
    sort_container.set_orientation(Gtk.Orientation.VERTICAL);

    def add_sort_option(name, val:MediaSorting, group=None):
        check = Gtk.CheckButton.new_with_label(name)
        check.value = val
   
        #if val == win.media_dir.sorting:
        #    check.set_active(True)

        if group:
            check.set_group(group)

        sort_container.append(check)
        check.connect("toggled", lambda *args: win.change_sorting(val))

        return check 
    menu_holder.add_sort_option = add_sort_option


    sort_container.append(Gtk.Label.new("Järjestys"))
    menu_holder.append(sort_container)

    group = menu_holder.add_sort_option("Nimi", MediaSorting.NAME)
    menu_holder.add_sort_option("Pääte",        MediaSorting.EXT,   group)
    menu_holder.add_sort_option("Tyyppi",       MediaSorting.TYPE,  group)
    menu_holder.add_sort_option("Muokkaus",     MediaSorting.MTIME, group)
    menu_holder.add_sort_option("Koko",         MediaSorting.SIZE,  group)
    menu_holder.add_sort_option("Avaus",        MediaSorting.ATIME, group)
    menu_holder.add_sort_option("Ctime",        MediaSorting.CTIME, group)

    menu_holder.append(Gtk.Separator.new(Gtk.Orientation.VERTICAL))

    show_hidden_toggle = Gtk.ToggleButton.new_with_label("Näytä piilotiedostot")
    menu_holder.append(show_hidden_toggle)

    def show_hiddens_toggled(toggle_button):
        win.toggle_display_hiddens(display=toggle_button.get_active())
        
    show_hidden_toggle.connect("toggled", show_hiddens_toggled)

    #reversed_order_toggle = Gtk.ToggleButton.new_with_label("Käänteinen järjestys")
    header.reversed_order_toggle.set_active(False)

    def show_reversed_toggled(reversed_button):
        win.toggle_reverse_order(reverse_sort=reversed_button.get_active())
    header.reversed_order_toggle.connect("toggled", show_reversed_toggled)

    menu_holder.append(header.reversed_order_toggle)
