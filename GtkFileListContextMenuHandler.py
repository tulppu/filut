
import gi

import GtkContextMenu
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gdk, GLib, GObject 

def _init(self):
    self.gesture_right_click = Gtk.GestureClick()
    self.gesture_right_click.set_button(3)
    self.add_controller(self.gesture_right_click)

    def right_clicked(gesture, num, x, y):
        child = self.get_child_at_pos(x, y)
        GtkContextMenu.pop_context_menu(self, child, x, y)
        return True

    self.gesture_right_click.connect("released", right_clicked)

    self.gesture_left_click = Gtk.GestureClick()
    self.gesture_left_click.set_button(1)
    self.add_controller(self.gesture_left_click)

    def left_clicked(gesture, num, x, y):
        return True
    self.gesture_left_click.connect("released", left_clicked)

    def key_pressed_cb(controller:Gtk.EventControllerKey, key:int, x:int, flags:Gdk.ModifierType):
        selected_childs_all = self.get_selected_children()
        if not len(selected_childs_all):
            return

        if Gdk.ModifierType.CONTROL_MASK % flags:
            return

        selected_child = selected_childs_all[0]
        selected_child = selected_child.get_next_sibling()

        char = chr(key)

        while selected_child:
            if selected_child.file_name[0] is char:

                self.unselect_all()
                self.select_child(selected_child)
                selected_child.grab_focus()
                break
            selected_child = selected_child.get_next_sibling()

    key_pressed_controller = Gtk.EventControllerKey.new()
    GObject.signal_connect_closure(key_pressed_controller, "key-pressed", key_pressed_cb, self)
    Gtk.Widget.add_controller(self, key_pressed_controller)


