import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk
from gi.repository import Gtk, GLib, Gio, Pango, Gdk




def init_mounter(win):

	volume_monitor = Gio.VolumeMonitor.get()
	#volume_monitor = Gio.VolumeMonitor.new()

	volume_monitor.connect("volume-changed", lambda *args: print(args))	
	volume_monitor.connect("volume-added", lambda *args: print(args))	
	volume_monitor.connect("mount-changed", lambda *args: print(args))	
	volume_monitor.connect("mount-added", lambda *args: print(args))	
	volume_monitor.connect("drive-changed", lambda *args: print(args))	
	volume_monitor.connect("drive-connected", lambda *args: print(args))	
	#volume_monitor.connect("drive-added", lambda *args: print(args))	

	print(volume_monitor.get_connected_drives())

	print("Mounts init'd")