
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, GLib, Gio,  Pango 

def update_footer(win, *args):
    tab = win.get_current_tab()
    media_dir = win.get_media_dir()

    footer = win.footer

    if not tab or not hasattr(tab, "current_page"):
        return


    info =  "Sivu "  + str(tab.current_page + 1) + "/" + str(media_dir.pages)
    info += ", tiedostoja " + str(media_dir.count) + "."

    footer.dir_info.set_text(info)

    current_page_num = tab.current_page
    
    footer.btn_next_page.set_sensitive(False)
    footer.btn_prev_page.set_sensitive(False)
    footer.btn_select_page.set_sensitive(False)

    if media_dir.pages > 1:
        footer.btn_select_page.set_sensitive(True)

    if current_page_num > 0:
        footer.btn_prev_page.set_sensitive(True)

    if media_dir.pages > current_page_num + 1:
        footer.btn_next_page.set_sensitive(True)


from GtkFileListItem import FilesListItem

def init_footer(win):

    footer = Adw.HeaderBar()
    footer.title = Gtk.Label()
    footer.set_title_widget(footer.title)
    footer.add_css_class("footer")

    #footer.set_show_title(False)

    footer.win = win
    win.footer = footer

    win.main_box.append(footer)


    footer.set_show_end_title_buttons(False)
    footer.set_show_start_title_buttons(False)
    #footer.set_title_widget(None)

    
    footer.btn_next_page = Gtk.Button(label="")
    # TOOD: Add dialog for choosing page num.
    footer.btn_select_page = Gtk.Button(label="")
    footer.btn_prev_page = Gtk.Button(label="")

    footer.btn_next_page.set_sensitive(False)
    footer.btn_select_page.set_sensitive(False)
    footer.btn_prev_page.set_sensitive(False)


    footer.btn_next_page.set_icon_name("go-next")
    footer.btn_next_page.add_css_class("btn-next-page")

    footer.btn_select_page.set_icon_name("media-optical-symbolic")
    footer.btn_select_page.add_css_class("btn-select-page")    

    footer.btn_prev_page.set_icon_name("go-previous")
    footer.btn_prev_page.add_css_class("btn-prev-page")

    footer.pack_start(footer.btn_prev_page)
    footer.pack_start(footer.btn_select_page)
    footer.pack_start(footer.btn_next_page)
   

    footer.dir_info = Gtk.Label()
    footer.pack_start(footer.dir_info)



    footer.btn_next_page.connect(
        "clicked", 
        lambda *args: win.change_to_page(win.get_current_page() + 1)
    )

    footer.btn_prev_page.connect(
        "clicked", 
        lambda *args: win.change_to_page(win.get_current_page() - 1)
    )

    win.connect(
            "page-changed", 
            lambda *args: update_footer(win, args)
    )


    win.tab_view.connect(
            "notify::selected-page", 
            lambda *args: update_footer(win, args)
    )


    footer.default_app_icon = Gtk.Image()
    footer.pack_end(footer.default_app_icon)

    footer.ani_thumb = None

    footer.file_info = Gtk.Box()
    footer.file_info.items = {}
    
    def add_info_item(name, value=None, tooltip=None, spacer_txt=" | ") -> Gtk.Label:
        item = Gtk.Label()
        footer.file_info.items[name] = item
        if value: 
            item.set_text(value)
        if tooltip:
            item.set_tooltip_text(tooltip)
        item.set_visible(False)
        #item.set_editable(False)
        item.set_selectable(True)

        spacer = Gtk.Label()
        if spacer_txt:
            spacer.set_text(spacer_txt)
        spacer.set_visible(False)

        item.spacer = spacer

        footer.file_info.append(item)
        footer.file_info.append(spacer)

        return item

    def set_info_item(item:Gtk.Entry, value:str, tooltip:str=None, bold=True):
        if not tooltip:
            tooltip = value
        item.set_tooltip_text(tooltip)
        if (bold):
            value = value 
            #value = "<b>" + value + "</b>"
        item.set_markup(value)
        item.set_visible(True)
        item.spacer.set_visible(True)

    def hide_info_item(item):
        item.set_visible(False)
        item.spacer.set_visible(False)

    def hide_info_bar():
        footer.file_info.set_visible(False)

    def show_info_bar():
        footer.file_info.set_visible(True)
    
    win.connect(
            "page-changed", 
            lambda *args: hide_info_bar()
    )

    info_name   = add_info_item(name="name")
    info_c_date = add_info_item(name="c_date")
    info_m_date = add_info_item(name="m_date")
    info_mime   = add_info_item(name="mime")
    info_size   = add_info_item(name="size")

    info_name.set_ellipsize(Pango.EllipsizeMode.START)
    info_name.set_max_width_chars(32)

    footer.pack_end(footer.file_info)
    
    def file_selected(files_box):

        footer.default_app_icon.clear()
        selected_childs = files_box.get_selected_children()

        show_info_bar()

        file_child:FilesListItem = selected_childs[0] if len(selected_childs) else None 
        if not file_child:
            return
        
        if not file_child.thumb:
            file_child.request_thumb(win=win)

        info = file_child.get_g_info(q="*")

        creation = info.get_creation_date_time()
        modification = info.get_modification_date_time()

        if (creation):
            set_info_item(
                            item      = info_c_date, 
                            value     = creation.to_local().format("%d.%m.%y"), 
                            tooltip   = "Luotu " + creation.to_local().format("%c")
            )
        else:
            hide_info_item(info_c_date)
        if (modification and not modification.equal(creation)):
            set_info_item(
                            item      = info_m_date, 
                            value     = modification.to_local().format("%d.%m.%y"),
                            tooltip   = "Muokattu " + modification.to_local().format("%c")
            )
        else:
            hide_info_item(info_mime)

        if GLib.DateTime.compare(creation, modification) == 0:
            hide_info_item(info_m_date)


        set_info_item(info_name, info.get_display_name())
        set_info_item(info_size, GLib.format_size(info.get_size()))
        mime = info.get_content_type()
        set_info_item(info_mime, mime)

        default_app = Gio.AppInfo.get_default_for_type(mime, False)

        if default_app:
            app_icon = default_app.get_icon()
            Gtk.Image.set_from_gicon(footer.default_app_icon, app_icon)


    def tab_created(args):
        tab = args[2]
        files = tab.files_box
        files.connect(
            "selected-children-changed",
            lambda *args: file_selected(args[0])
        )

    win.connect(
            "tab-added", 
            lambda *args: tab_created(args)
    )
