
from typing import List
import GtkFileListContextMenuHandler
import GtkFileListFilter
import GtkFileListItem
import MediaDir

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gdk, GLib, GObject 
import Drags


_file_added_cbs = []

class FileList(Gtk.FlowBox):

    #items = []
    container:Gtk.ScrolledWindow = None 

    #lock = Lock() 
    window:Gtk.Window = None
    tab_item:Adw.TabPage = None
    media_dir:MediaDir = None
    tab = None
    filter_entry:Gtk.Entry = None

    gesture_right_click = Gtk.GestureClick()
    context_menu = None


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.container = Gtk.ScrolledWindow()
        self.overlay = Gtk.Overlay()
        self.overlay.set_child(self.container);        

        GtkFileListContextMenuHandler._init(self)

        overlay_content = Gtk.Box()
        overlay_content.set_orientation(Gtk.Orientation.HORIZONTAL)
        overlay_content.add_css_class("overlay")
        overlay_content.set_halign(Gtk.Align.END)
        overlay_content.set_valign(Gtk.Align.END)
        self.overlay.content = overlay_content

        # self.__add_filter()
        GtkFileListFilter._add_filter(self)

        self.overlay.add_overlay(overlay_content)
        self.overlay.set_measure_overlay(overlay_content, False)

        self.set_activate_on_single_click(False)
        self.set_valign(Gtk.Align.START)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.set_size_request(800, -1)
        self.set_vexpand(True)
        self.set_hexpand(True)
        self.set_homogeneous(True)
        self.set_selection_mode(Gtk.SelectionMode.MULTIPLE)

        self.container.set_child(self)
        self.container.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        self.container.set_vexpand(True)
        self.container.set_hexpand(True)

        self.connect("child-activated", lambda box, child: child.open_file())

        self.drag_source = Gtk.DragSource.new()
        self.drag_source.connect("prepare", Drags.drag_prepare, self)
        self.drag_source.connect("drag-begin", Drags.drag_begin, self)

        self.add_controller(self.drag_source)

        content = Gdk.ContentFormats.new_for_gtype(Gdk.FileList)

        drop_target = Gtk.DropTarget(formats=content, actions=Gdk.DragAction.COPY)
        drop_target.parent = self

        drop_target.connect("drop", Drags.files_on_drop) 

        self.add_controller(drop_target)


        #self.container.connect("activate", lambda* args:("test"))


    def get_child_by_path(self, path:str):
        child:GtkFileListItem.FilesListItem= self.get_child_at_index(0)
        while child != None:
            if child.media_file.get_path() == path:
                return child
            child = child.get_next_sibling()

    def get_filter_string(self):
        return self.filter_entry.get_buffer().get_text()

    def set_filter_string(self, str):
        return self.filter_entry.get_buffer().set_text(str)

    def get_media_dir(self):
        return self.tab.media_dir

    def get_window(self):
        return self.tab.window

    def append(self, child:GtkFileListItem.FilesListItem):
        global _file_added_cbs
        super().append(child)
        trigger_file_added_cbs(self, child)

    def append_many(self, childs:List[GtkFileListItem.FilesListItem]):
        for c in childs:
            self.append(c)

    def clear(self, cb=None):
        #self.lock.acquire()
        while True:
            c = self.get_child_at_index(0)
            if not c:
                break
            c.clear()
            self.remove(c)
            #Gtk.FlowBox.remove(self, c)
            c.run_dispose()
            c = None



        #self.lock.release()
        if cb:
            cb()

def register_file_added_cb(cb:callable):
    global _file_added_cbs
    _file_added_cbs.append(cb)

def unregister_file_added_cb(cb:callable):
    global _file_added_cbs
    #_file_added_cbs = _file_added_cbs.
    _file_added_cbs.remove(cb)

def trigger_file_added_cbs(files_box:FileList, files_box_child:GtkFileListItem.FilesListItem):
    global _file_added_cbs
    for cb in _file_added_cbs:
        cb(files_box, files_box_child)

